/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('UApps.pages', [
        'ui.router',


        'UApps.pages.questions',
        'UApps.pages.templates',
        'UApps.pages.outlets',
        'UApps.pages.feedback',
        'UApps.pages.devices',
        'UApps.pages.settings',
        'UApps.pages.feedbackView',
        'UApps.pages.users',
        'UApps.pages.reports',
        'UApps.pages.dashboard'


    ])
        .config(routeConfig).run(['$rootScope', '$location', 'AuthenticationService', 'toastr',
        function ($rootScope, $location, AuthenticationService, toastr) {
            // keep user logged in after page refresh
            $rootScope.isFeedbackView = false;
            //Read authentication from cookies
            AuthenticationService.init();

            if (AuthenticationService.authenticate()) {
                var cookieUser = $rootScope.globals.currentUser;
                AuthenticationService.getUserInfo().then(function (response) {
                    if (response.menuAccess != cookieUser.roles.menu || response.outletAccess != cookieUser.outletAccess) {
                        $location.path('/login');
                        AuthenticationService.logout().then(function () {
                            AuthenticationService.clearCredentials();
                            $location.path('/login');
                            toastr.info("Your privileges has been changed.", "Invalid session!");
                        });
                    }
                });
            }
            $rootScope.$on('$stateChangeStart', function (event, toState, toStateParams) {
                // Check if current user has group access
                var menuId = angular.isDefined(toState.data) ? toState.data.menuId : 0;
                if (!AuthenticationService.userHasMenuAccess(menuId) && $location.path().indexOf("/feedbackView") == -1) {
                    $location.path('/');
                }
            });

            $rootScope.$on('$locationChangeStart', function (event, next, current) {
                setPageLocation();
            });
            setPageLocation();

            function setPageLocation() {
                $rootScope.isFeedbackView = false;
                if ($location.path().indexOf("/feedbackView") >= 0) {
                    $rootScope.isFeedbackView = true;
                } else {
                    if (AuthenticationService.authenticate()) {
                        if ($location.path() == '/login' || $location.path() == "" || $location.path() == '/') {
                            $location.path('/dashboard');
                        }
                    } else {
                        if ($location.path() !== '/login' && $location.path() !== '/register') {
                            $location.path('/login');
                        }
                    }
                }
            }

            /*     function setPageLocation() {
             $rootScope.isFeedbackView = false;
             if (AuthenticationService.authenticate()) {
             if ($location.path() == '/login' || $location.path().indexOf("/feedbackView") >= 0 || $location.path() == "") {
             $location.path('/');
             }
             } else {
             if ($location.path().indexOf("/feedbackView") >= 0) {
             $rootScope.isFeedbackView = true;
             }
             // redirect to login page if not logged in
             else if ($location.path() !== '/login')
             $location.path('/login');
             }
             }*/
        }]);

    /** @ngInject */
    function routeConfig($urlRouterProvider, menusList) {
        $urlRouterProvider.otherwise(function ($injector, $location) {
            var state = $injector.get('$state');
            var authService = $injector.get('AuthenticationService');
            if (authService.authenticate()) {
                $urlRouterProvider.otherwise("/dashboard");
                /* var userMenuIdsList = authService.getUserRole().menu.split(",");
                 for (var i = 0; i < userMenuIdsList.length; i++) {
                 var menuObj = getMenuItem(menusList, userMenuIdsList[i]);
                 if (!menuObj.isAbstract) {
                 return $location.path(menuObj.hyperlink);
                 }
                 }*/
            }

        });
    }

    function getMenuItem(arr, id) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].id == id) {
                return arr[i];
            }
        }
    }

})();
