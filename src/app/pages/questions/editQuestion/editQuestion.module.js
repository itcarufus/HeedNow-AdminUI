/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('UApps.pages.questions')
        .config(routeConfig).controller('editQuestionCtrl', editQuestionCtrl);

    function editQuestionCtrl($location, $rootScope, binarySelectSymbolTypes, singleSelectSymbolTypes, symbolTypes2, $stateParams, Question, $filter, QuestionsData, toastr, $scope, symbolTypes, questionAnswerTypes) {

        $scope.questionListsData = $scope.questionListsMasterData = QuestionsData.getList();
        var editModalBox;
        $scope.itHasParentQuestion = false;
        $scope.itsOpenText = false;
        $scope.QuestionTypeOptions = questionAnswerTypes;
        //$scope.SymbolTypeOptions = symbolTypes;

        /*  $scope.editQuestion = {
         form: {},
         info: Question.newObject()
         };*/

        $scope.selected = {
            parentQuestion: {},
            parentAnswer: {}
        };

        //$scope.questionInfo = QuestionsData.getRowById($stateParams.questionId);


        $scope.editQuestion = {
            form: {},
            info: {
                id: "",
                questionDesc: "",
                questionType: "",
                parentAnswerId: "",
                parentQuestionId: "",
                answerSymbol: "",
                answerOption: [],
                threshold: "",
            },
            ratingThreshold: ""
        };
        $scope.isLoadingParentAnswers = false;
        $scope.getParentAnswerByQuestionId = function () {
            $scope.isLoadingParentAnswers = false;
            if ($scope.selected.parentQuestion != null) {
                QuestionsData.answerByQuestion($scope.selected.parentQuestion.id).then(function (response) {
                    $scope.answerListByQuestion = response;
                    toastr.info("Select  parent answer from given answer list.")
                    if ($scope.answerListByQuestion.length != 0) {
                        $scope.isLoadingParentAnswers = true;
                    }
                });
            }
        };
        $scope.addRow = function (index) {
            var option = {answerDesc: "", rating: ""};
            if ($scope.editQuestion.info.answerOption.length <= index + 1) {
                $scope.editQuestion.info.answerOption.splice(index + 1, 0, option);
            }
        };

        $scope.deleteRow = function ($event, index) {
            if ($event.which == 1)
                $scope.editQuestion.info.answerOption.splice(index, 1);
        };

        var parentQuestionId;
        getQuestionInfo();
        function getQuestionInfo() {
            QuestionsData.questionInfo($stateParams.questionId).then(function (response) {
                for (var i = 0; i < response.options.length; i++) {
                    var ansObj = response.options[i];
                    $scope.editQuestion.ratingThreshold = ansObj.threshold != "" ? parseInt(ansObj.threshold) : ansObj.threshold;
                    break;
                }

                $scope.questionInfo = response;
                checkParentQuestion();
                checkAnswerSymbols();
                $scope.editQuestion.info = $scope.questionInfo;
                $scope.editQuestion.info.answerOption = $scope.questionInfo.options;
                parentQuestionId = $scope.questionInfo.parentQuestionId;
                getAnswerOptions();
                $scope.selected.parentQuestion = QuestionsData.getRowById($scope.questionInfo.parentQuestionId);
                //$scope.selected.parentAnswer =  QuestionsData.getRowById($scope.questionInfo.parentAnswerId);
            });
        }

        $scope.isSingleRating = false;
        $scope.itsSingleSelect = false;
        $scope.isRatingQues = false;
        function checkParentQuestion() {
            if ($scope.questionInfo.parentQuestionId != 0) {
                $scope.itHasParentQuestion = true;
            }
            if ($scope.questionInfo.questionType == 4) {
                $scope.itsOpenText = true;
            }
            if ($scope.questionInfo.questionType == 2) {//for single rating symbol option setting and enter option disabled only enter rating and do proceed
                $scope.SymbolTypeOptions = symbolTypes2;
                $scope.isSingleRating = true;
            } else {
                $scope.isSingleRating = false;
                $scope.SymbolTypeOptions = symbolTypes;
            }
            if ($scope.questionInfo.questionType == 2 || $scope.questionInfo.questionType == 3) {//for single and mltiple rating their is only 2 answerSymbols
                $scope.SymbolTypeOptions = symbolTypes2;
            } else {
                $scope.SymbolTypeOptions = symbolTypes;
            }
            $scope.isRatingQues = ($scope.questionInfo.questionType == 2 || $scope.questionInfo.questionType == 3);
        }

        function checkAnswerSymbols() {
            if ($scope.questionInfo.questionType == 1 || $scope.questionInfo.questionType == 5) {
                $scope.SymbolTypeOptions = singleSelectSymbolTypes;
                $scope.itsSingleSelect = true;
            } else if ($scope.questionInfo.questionType == 6) {
                $scope.SymbolTypeOptions = binarySelectSymbolTypes;
                $scope.itsSingleSelect = true;
            }
        }

        $scope.$watch("editQuestion.ratingThreshold", function (a, b) {
            if (angular.isDefined($scope.questionInfo) && angular.isDefined($scope.questionInfo.answerOption) && ($scope.questionInfo.questionType == 2 || $scope.questionInfo.questionType == 3)) {
                for (var i = 0; i < $scope.questionInfo.answerOption.length; i++) {
                    if (a != "") {
                        $scope.questionInfo.answerOption[i].threshold = a;
                    }
                }
            }
        });

        $scope.goToQuestionListPage = function () {
            $location.path("/questions");
        };

        $scope.gotoTemplatePage = function () {
            $location.path("/templates");
        };
        /*      $scope.noParentQuestion=false;
         $scope.noParentAnswer=false;
         $scope.setParentTQuestion= function () {
         if ($scope.questionInfo.parentQuestionId==0||$scope.questionInfo.parentQuestionId==0) {
         $scope.noParentQuestion = true;
         $scope.noParentAnswer = true;
         }
         };*/


        function getAnswerOptions() {
            if ($scope.questionInfo.parentQuestionId != null && $scope.questionInfo.parentQuestionId != 0) {
                QuestionsData.answerByQuestion(parentQuestionId).then(function (response) {
                    $scope.answerListByQuestion = response;
                    var parentAnswerId = $filter('filter')($scope.answerListByQuestion, {answer_id: $scope.questionInfo.parentAnswerId});
                    $scope.selected.parentAnswer = parentAnswerId[0];
                    $scope.isLoadingParentAnswers = true;
                    //console.info(parentAnswerId);
                });
            }
        }

        $scope.updateQuestion = function (isValid) {
            $scope.editQuestion.info = $scope.questionInfo;
            if (angular.isDefined($scope.selected.parentQuestion) && $scope.selected.parentQuestion != null) {
                $scope.editQuestion.info.parentQuestionId = $scope.selected.parentQuestion.id;
                $scope.editQuestion.info.parentAnswerId = $scope.selected.parentAnswer.answer_id;
            } else {
                $scope.editQuestion.info.parentQuestionId = 0;
                $scope.editQuestion.info.parentAnswerId = 0
            }
            if (isValid) {
                QuestionsData.update(Question.editFromObject($scope.editQuestion.info)).then(function () {
                    toastr.success("Question updated successfully!", "Success");
                    $scope.questionListsMasterData = QuestionsData.getList();
                    $scope.questionListsData = [].concat($scope.questionListsMasterData);

                    $location.path("/questions");
                }, function (errorMsg) {
                    toastr.error(errorMsg, "Failed");
                });
            }
        };

    }


    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('editQuestion', {
                url: '/editQuestion/:questionId',
                templateUrl: 'app/pages/questions/editQuestion/editQuestion.html',
                controller: editQuestionCtrl,
                title: 'Edit Question',
                data: {
                    menuId: 12
                },
                resolve: {
                    "LoadQuestions": function (QuestionsData) {
                        return QuestionsData.load();
                    }
                }
            })

    }

})();
