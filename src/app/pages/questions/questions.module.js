/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('UApps.pages.questions', [])
        .config(routeConfig).controller('questionsCtrl', questionsCtrl);
   /*     .filter('encodeURIComponent', function () {
            return window.encodeURIComponent;
        })
        .filter('decodeURIComponent', function () {
            return window.decodeURIComponent;
        });
*/
    function questionsCtrl($location, $rootScope, binarySelectSymbolTypes, Question, singleSelectSymbolTypes, symbolTypes2, AuthenticationService, QuestionsData, toastr, $scope, symbolTypes, questionAnswerTypes) {

        $scope.questionListsData = $scope.questionListsMasterData = QuestionsData.getList();
        var editModalBox;
        $scope.questionPageSize = 10;

        $scope.gotoCreateQuestion = function () {
            $location.path("/createQuestion");
        };
        $scope.hasAddRights = AuthenticationService.userHasMenuAccess(11);
        $scope.hasEditRights = AuthenticationService.userHasMenuAccess(12);
        $scope.QuestionTypeOptions = questionAnswerTypes;
        $scope.SymbolTypeOptions = symbolTypes;

        $scope.selected = {
            parentQuestion: {},
            parentAnswer: {}
        };

        /*  $scope.newQuestion = {
         form: {},
         info: Question.newObject()
         };
         */


        $scope.newQuestion = {
            form: {},
            info: {
                questionDesc: "",
                questionType: "",
                parentAnswerId: "",
                parentQuestionId: "",
                answerOption: [{
                    label: "",
                    rating: "",
                    weightage: "",
                    threshold: ""
                }],
                answerSymbol: "",
            },
            ratingThreshold: ""
        };
        $scope.itsOpenText = false;
        $scope.isSingleRating = false;
        $scope.isRatingQues = false;
        $scope.isSingleSelect = false;

        $scope.getQuestionType = function () {
            if ($scope.newQuestion.info.questionType == 4) {//if questionType is open text than answersSymbol must be text only
                $scope.newQuestion.info.answerSymbol = 4;
                $scope.itsOpenText = true;
                return true;
            }
            if ($scope.newQuestion.info.questionType == 2) {//for single rating symbol option setting and enter option disabled only enter rating and do proceed
                $scope.isSingleRating = true;
            } else {
                $scope.isSingleRating = false;
            }
            if ($scope.newQuestion.info.questionType == 1 || $scope.newQuestion.info.questionType == 5 || $scope.newQuestion.info.questionType == 6) {//for single rating symbol option setting and enter option disabled only enter rating and do proceed
                $scope.isSingleSelect = true;
            } else {
                $scope.isSingleSelect = false;
            }
            $scope.isRatingQues = ($scope.newQuestion.info.questionType == 2 || $scope.newQuestion.info.questionType == 3);
        };

        $scope.setAnswerSymbol = function () {
            $scope.newQuestion.info.answerSymbol = "";//every time make answerSymbol empty
            $scope.itsOpenText = false;
            if ($scope.newQuestion.info.questionType == 4) {
                $scope.newQuestion.info.answerSymbol = 4;

            } else if ($scope.newQuestion.info.questionType == 1 || $scope.newQuestion.info.questionType == 5) {//if its single select then answerSymbol must be Circle
                $scope.newQuestion.info.answerSymbol = 3;
                $scope.SymbolTypeOptions = singleSelectSymbolTypes;
            } else if ($scope.newQuestion.info.questionType == 6) {//for binary questionType answerSymbol must be tick/cross
                $scope.newQuestion.info.answerSymbol = 5;
                $scope.SymbolTypeOptions = binarySelectSymbolTypes;
            } else if ($scope.newQuestion.info.questionType == 6) {//if its single select then answerSymbol must be Circle
                $scope.newQuestion.info.answerSymbol = 5;
            } else if ($scope.newQuestion.info.questionType == 3 || $scope.newQuestion.info.questionType == 2) {//for multiple rating symbol option setting
                toastr.info("For single rating you should add weightage and rating also");
                $scope.SymbolTypeOptions = symbolTypes2;
            } else {
                $scope.SymbolTypeOptions = symbolTypes;
            }
        };


        $scope.$watch("newQuestion.ratingThreshold", function (a, b) {
            if (angular.isDefined($scope.newQuestion.info)) {
                for (var i = 0; i < $scope.newQuestion.info.answerOption.length; i++) {
                    if (a != "") {
                        $scope.newQuestion.info.answerOption[i].threshold = a;
                    }
                }
            }
        });

        $scope.addRow = function (index) {
            var option = {label: "", rating: "", weightage: ""};
            if ($scope.newQuestion.info.answerOption.length <= index + 1) {
                $scope.newQuestion.info.answerOption.splice(index + 1, 0, option);
            }
        };

        $scope.deleteRow = function ($event, index) {
            if ($event.which == 1)
                $scope.newQuestion.info.answerOption.splice(index, 1);
        };


        $scope.createQuestion = function (isValid) {
            if (isValid) {
                $scope.newQuestion.info.parentQuestionId = $scope.selected.parentQuestion.id;
                $scope.newQuestion.info.parentAnswerId = $scope.selected.parentAnswer.answer_id;
                QuestionsData.create(Question.createFromObject($scope.newQuestion.info)).then(function (newdata) {
                    toastr.success("Question created successfully!", "Success");
                    $scope.newQuestion.form.$setPristine();
                    $scope.newQuestion.info = Question.newObject();
                    $scope.questionListsMasterData = newdata;
                    $scope.questionListsData = [].concat($scope.questionListsMasterData);
                    $location.path("/questions");
                }, function (errorMsg) {
                    toastr.error(errorMsg, "Failed");
                });
            }
        };
        $scope.isLoadingParentAnswers = false;
        $scope.checkAnswersTypeForQuestion = function () {
            $scope.isLoadingParentAnswers = false;
            if ($scope.selected.parentQuestion != null) {
                QuestionsData.answerByQuestion($scope.selected.parentQuestion.id).then(function (response) {
                    toastr.info("Select  parent answer from given answer list.");
                    $scope.answerListByQuestion = response;
                    if ($scope.answerListByQuestion.length != 0) {
                        $scope.isLoadingParentAnswers = true;
                    }
                });

            }
        };

        $scope.goToQuestionListPage = function () {
            $location.path("/questions");
        };

        $scope.deleteQuestion = function (id) {
            QuestionsData.delete(id).then(function () {
                toastr.success("Question deleted successfully!", "Success");
                $scope.questionListsMasterData = QuestionsData.getList();
                $scope.questionListsData = [].concat($scope.questionListsMasterData);
            }, function (errorMsg) {
                toastr.error(errorMsg, "Failed");
            });
        };

        $scope.gotoTemplatePage = function (courseId) {
            $location.path("/templates");
        };

        $scope.editQuestionData = function (item) {
            $location.path("editQuestion/" + item.id);
        };

        /* $scope.updateQuestion = function (isValid) {
         if (isValid) {
         QuestionsData.update($scope.editQuestion.info).then(function () {
         toastr.success("Question updated successfully!", "Success");
         $scope.questionListsMasterData = QuestionsData.getList();
         $scope.questionListsData = [].concat($scope.questionListsMasterData);
         editModalBox.close();
         }, function (errorMsg) {
         toastr.error(errorMsg, "Failed");
         });
         }
         };*/

    }


    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider

            .state('questions', {
                url: '/questions',
                controller: questionsCtrl,
                templateUrl: 'app/pages/questions/questions.list.html',
                title: 'QUESTION BANK',
                data: {
                    menuId: 1
                },
                sidebarMeta: {
                    icon: 'fa fa-question',
                    order: 2
                },
                resolve: {
                    "LoadQuestions": function (QuestionsData) {
                        return QuestionsData.load();
                    }
                }
            })

            .state('createQuestion', {
                url: '/createQuestion',
                templateUrl: 'app/pages/questions/createQuestion.html',
                controller: questionsCtrl,
                title: 'CREATE QUESTION',
                data: {
                    menuId: 11
                },
                resolve: {
                    "LoadQuestions": function (QuestionsData) {
                        return QuestionsData.load();
                    }
                }
            })
    }

})();
