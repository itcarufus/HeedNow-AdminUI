(function () {
    'use strict';

    angular.module('UApps.pages.settings', [])
        .config(routeConfig).controller('SettingsCtrl', SettingsCtrl);
    function SettingsCtrl($scope, Excel, $timeout, _Setting, AuthenticationService, OutletsData, $filter, menusList, Role, SettingsData, RolesData, toastr, menuInfoList, $uibModal, $window) {

        $scope.outletListsData = $scope.outletListsMasterData = OutletsData.getList();
        $scope.roleListData = $scope.roleListMasterData = RolesData.getList();
        $scope.hasGeneralEditAccess = AuthenticationService.userHasMenuAccess(20);
        $scope.hasSMSGatewayEditAccess = AuthenticationService.userHasMenuAccess(21);
        $scope.hasRolesAddEditAccess = AuthenticationService.userHasMenuAccess(22);
        $scope.hasAddGatewayAccess = AuthenticationService.userHasMenuAccess(30);
        getSmsSetting();
        var settingPageSize = 10;
        $scope.menusList = menusList;
        $scope.getAccessCount = function (value) {
            var tempArr = value.split(",");
            return tempArr.length
        };
        $scope.getLevelStyle = function (seqId) {
            return {'padding-left': seqId.split(".").length * 15 + 'px'}
        };


        //getMenuList();
        function getMenuList() {
            SettingsData.menuList().then(function (response) {
                $scope.menusList = response;
                /*    var tempArr = response;
                 var parentObjs = [];
                 var i = 0;
                 while (tempArr.length > 0) {
                 var obj = getObjById(tempArr[i].id)
                 if (obj == null && obj.parentId != 0) {

                 }
                 }*/

            });
        }

        /*
         function getObjById(arr, parentId) {
         for(var i=0;i<arr.length;i++){
         if(arr.le)
         }
         }*/
        $scope.settings = {
            form: {},
            info: {}
        };
        $scope.smsSettings = {
            form: {},
            info: {}
        };
        $scope.createSmsSettings = {
            form: {},
            info: {}
        };
        function getSmsSetting() {
            SettingsData.fetchSmsSetting().then(function (response) {
                $scope.smsSettingMasterInfo = response;
                $scope.smsSettingInfo = [].concat($scope.smsSettingMasterInfo);
            });
        }

        //var settingsData = angular.copy(SettingsData.getData());
        var loadingText;
        getGeneralSetting();
        function getGeneralSetting() {
            SettingsData.fetch().then(function (response) {
                var settingsData = angular.copy(response);
                if (angular.isDefined(settingsData)) {
                    var timeSplits = settingsData.archiveTime.split(":");
                    var timeSplits2 = settingsData.reportTime.split(":");
                    var dt = new Date(1970, 0, 1, timeSplits[0], timeSplits[1], 0);
                    var dt2 = new Date(1970, 0, 1, timeSplits2[0], timeSplits2[1], 0);
                    settingsData.archiveTime = dt;
                    settingsData.reportTime = dt2;
                    $scope.settings.info = settingsData;
                }
            }, function () {

            });
        }

        $scope.saveSettings = function (isValid) {
            if (isValid) {
                SettingsData.saveSettings(_Setting.saveObject($scope.settings.info)).then(function () {
                    toastr.success("Settings saved successfully!", "Success");
                }, function (errorMsg) {
                    toastr.error(errorMsg, "Failed");
                });
            }
        };

        $scope.saveSmsSettings = function (isValid) {
            if (isValid) {
                SettingsData.saveSmsSettings($scope.smsSettings.info).then(function () {
                    toastr.success("Sms settings saved successfully!", "Success");
                }, function (errorMsg) {
                    toastr.error(errorMsg, "Failed");
                });
            }
        };
        $scope.addSmsSettings = function (isValid) {
            if (isValid) {
                SettingsData.saveSmsSettings($scope.createSmsSettings.info).then(function () {
                    toastr.success("New sms settings added successfully!", "Success");
                    getSmsSetting();
                    createGateWayModal.close();
                }, function (errorMsg) {
                    toastr.error(errorMsg, "Failed");
                });
            }
        };
        $scope.editSmsGateway = {
            form: {},
            info: {}
        };
        $scope.updateSmsSettings = function () {
            SettingsData.updateSmsSettings($scope.editSmsGateway.info).then(function () {
                toastr.success("Data updated successfully!", "Success");
                editSmsDetailsInfoModal.close();
            }, function (errorMsg) {
                toastr.error(errorMsg, "Failed");
            });
        };
        var editSmsDetailsInfoModal;
        $scope.editSmsDetailsInfoBox = function (item) {
            $scope.editSmsGateway.info = item;
            editSmsDetailsInfoModal = $uibModal.open({
                animation: true,
                templateUrl: 'app/pages/settings/edit.smsGatewayInfo.html',
                size: 'md',
                backdrop: 'static',
                keyboard: false,
                scope: $scope
            });
        };
        var viewMenuInfoModal;
        $scope.openMenuInfoBox = function () {
            $scope.menuInfoList = menuInfoList;
            viewMenuInfoModal = $uibModal.open({
                animation: true,
                templateUrl: 'app/pages/settings/view.menuInfo.html',
                size: 'lg',
                backdrop: 'static',
                keyboard: false,
                scope: $scope
            });
        };
        var createGateWayModal;
        $scope.createGatewayPopup = function () {
            createGateWayModal = $uibModal.open({
                animation: true,
                templateUrl: 'app/pages/settings/createSmsGatewaySetting.html',
                size: 'md',
                backdrop: 'static',
                keyboard: false,
                scope: $scope
            });
        };
        $scope.newRole = {
            form: {},
            info: {}
        };
        $scope.selectAllOutlet = {
            isChecked: false
        };
        $scope.editSelectAllOutlet = {
            isChecked: false
        };
        $scope.selected = {
            menu: [],
            outlet: {}
        };
        $scope.newSelected = {
            menu: [],
            outlet: {}
        };

        $scope.menu = [];
        $scope.createRole = function (isValid) {
            if (isValid) {
                if ($scope.selected.outlet.length != 0 || $scope.selectAllOutlet.isChecked) {
                    if ($scope.selected.menu.length != 0) {
                        $scope.selected.menu = $filter('orderBy')($scope.selected.menu, 'id', false);
                        var menuId = [];
                        var outletId = [];
                        var isAll;
                        for (var i = 0; i < $scope.selected.menu.length; i++) {
                            menuId.push($scope.selected.menu[i].id)
                        }
                        if ($scope.selectAllOutlet.isChecked) {
                            for (var j = 0; j < $scope.outletListsData.length; j++) {
                                outletId.push($scope.outletListsData[j].id);
                                isAll = 1;
                            }
                        } else {
                            for (var j = 0; j < $scope.selected.outlet.length; j++) {
                                outletId.push($scope.selected.outlet[j].id);
                                isAll = 0;
                            }
                        }

                        var menuStr = menuId.toString();
                        var outletStr = outletId.toString(outletId);
                        RolesData.create(Role.createObject($scope.newRole.info.name, menuStr, outletStr, isAll)).then(function () {
                            toastr.success("Role created successfully!", "Success");
                            RolesData.load().then(function () {
                                $scope.roleListMasterData = RolesData.getList();
                                $scope.roleListData = [].concat($scope.roleListMasterData);
                            });
                            $scope.newRole.form.$setPristine();
                            $scope.newRole.info = "";
                            $scope.selected.menu = [];
                            $scope.selected.outlet = {};
                            $scope.selectAllOutlet.isChecked = false;
                        }, function (errorMsg) {
                            toastr.error(errorMsg, "Failed");
                        });
                    } else {
                        alert("Please select at least one menu")
                    }
                } else {
                    alert("Please select at least one outlet")
                }
            }
        };
        var editRoleModalBox;
        $scope.editRoleInfo = {
            form: {},
            info: {}
        };
        $scope.editRoleData = function (item) {
            var outletIdArr = item.outletAccess.split(",");
            /*   if (outletIdArr.length == $scope.outletListsData.length && item.outletAccess.length > 0) {
             $scope.selectAllOutlet.isChecked = true;
             } else {
             $scope.selectAllOutlet.isChecked = false;
             } */
            if (item.isAll == 1) {
                $scope.editSelectAllOutlet.isChecked = true;
            } else {
                $scope.editSelectAllOutlet.isChecked = false;
            }
            $scope.editRoleInfo.info = item;
            var tempOutletArr = [];
            for (var i = 0; i < outletIdArr.length; i++) {
                var outletIndex = getItemIndex($scope.outletListsData, outletIdArr[i]);
                if (outletIndex !== -1)
                    tempOutletArr.push($scope.outletListsData[outletIndex]);
            }
            $scope.newSelected.outlet = tempOutletArr;

            var tempMenuArr = [];
            $scope.tempMenuObj = {};
            var menuIdArr = item.menuAccess.split(",");
            for (var i = 0; i < menuIdArr.length; i++) {
                var menuIndex = getItemIndex($scope.menusList, menuIdArr[i]);
                if (menuIndex !== -1)
                    tempMenuArr.push($scope.menusList[menuIndex]);
            }
            $scope.newSelected.menu = tempMenuArr;
            editRoleModalBox = $uibModal.open({
                animation: true,
                templateUrl: 'app/pages/settings/edit.role.html',
                size: 'md',
                backdrop: 'static',
                keyboard: false,
                scope: $scope
            });
        };

        var viewAssignedOutletsModal;
        $scope.outletLoaded = false;
        $scope.viewAssignedOutletBox = function (item) {
            $scope.tempOutletObj = {};
            var tempOutletArr = [];

            var outletIdArr = item.outletAccess.split(",");
            for (var i = 0; i < outletIdArr.length; i++) {
                var outletIndex = getItemIndex($scope.outletListsData, outletIdArr[i]);
                if (outletIndex !== -1)
                    tempOutletArr.push($scope.outletListsData[outletIndex]);
            }
            $scope.newSelected.outlet = tempOutletArr;
            $scope.outletLoaded = true;

            viewAssignedOutletsModal = $uibModal.open({
                animation: true,
                templateUrl: 'app/pages/settings/view.assignedOutlets.html',
                size: 'md',
                backdrop: 'static',
                keyboard: false,
                scope: $scope
            });
        };

        $scope.validParentMenu = function (item, checked, $listModel, isManual) {
            var itemParentId = item.parentId;
            var parentItem = $filter('filter')($scope.menusList, {id: itemParentId}, true)[0];
            var childItems = $filter('filter')($scope.menusList, {parentId: item.id}, true);
            var pidx = -1;
            var cidx = -1;
            var allSiblings = $filter('filter')($scope.menusList, {parentId: itemParentId}, true);
            if (checked) {
                if (itemParentId > 0) {
                    pidx = getItemIndex($listModel, itemParentId);
                    if (pidx === -1) {
                        $listModel.push(parentItem);
                        $scope.validParentMenu(parentItem, true, $listModel, true);
                    }

                }
                cidx = getItemIndex($listModel, item.id);
                if (cidx === -1)
                    $listModel.push(item);

                if (angular.isUndefined(isManual) || !isManual) {
                    for (var j = 0; j < childItems.length; j++) {
                        if (childItems[j].id != item.id && getItemIndex($listModel, childItems[j].id) === -1) {
                            $listModel.push(childItems[j]);
                        }
                    }
                }

            }

            if (!checked) {
                var anyChildSelected = false;
                for (var i = 0; i < allSiblings.length; i++) {
                    if (allSiblings[i].id != item.id && getItemIndex($listModel, allSiblings[i].id) !== -1) {
                        anyChildSelected = true;
                        break;
                    }
                }
                pidx = getItemIndex($listModel, itemParentId);
                if (!anyChildSelected && pidx >= 0 && parentItem.isAbstract) {
                    $scope.validParentMenu(parentItem, false, $listModel, true);
                    $listModel.splice(pidx, 1);
                }
                cidx = getItemIndex($listModel, item.id);
                if (cidx >= 0) {
                    $listModel.splice(cidx, 1);
                }

                for (var k = 0; k < childItems.length; k++) {
                    var chidx = getItemIndex($listModel, childItems[k].id);
                    if (chidx >= 0) {
                        $scope.validParentMenu(childItems[k], false, $listModel, true);
                        $listModel.splice(chidx, 1);
                    }
                }
            }

        };
        function getItemIndex(arr, id) {
            for (var i = 0; i < arr.length; i++) {
                if (arr[i].id == id) {
                    return i;
                }
            }
            return -1;
        }

        $scope.updateRole = function (isValid) {
            if (isValid) {
                if ($scope.newSelected.outlet.length != 0) {
                    if ($scope.newSelected.menu.length != 0) {
                        $scope.newSelected.menu = $filter('orderBy')($scope.newSelected.menu, 'id', false);
                        var menuId = [];
                        var outletId = [];
                        var isAll;
                        for (var i = 0; i < $scope.newSelected.menu.length; i++) {
                            menuId.push($scope.newSelected.menu[i].id)
                        }
                        if ($scope.editSelectAllOutlet.isChecked) {
                            for (var j = 0; j < $scope.outletListsData.length; j++) {
                                outletId.push($scope.outletListsData[j].id)
                                isAll = 1;
                            }
                        } else {
                            for (var j = 0; j < $scope.newSelected.outlet.length; j++) {
                                outletId.push($scope.newSelected.outlet[j].id)
                                isAll = 0;
                            }
                        }
                        var menuStr = menuId.toString();
                        var outletStr = outletId.toString();
                        RolesData.update(Role.updateObject($scope.editRoleInfo.info, menuStr, outletStr, isAll)).then(function () {
                            toastr.success("Role updated successfully!", "Success");
                            RolesData.load().then(function () {
                                $scope.roleListMasterData = RolesData.getList();
                                $scope.roleListData = [].concat($scope.roleListMasterData);
                            });
                            editRoleModalBox.close();
                            $scope.editRoleInfo.form.$setPristine();
                            $scope.editRoleInfo.info = "";
                            $scope.newSelected.menu = [];
                            $scope.newSelected.outlet = {};
                            $scope.editSelectAllOutlet.isChecked = false;
                        }, function (errorMsg) {
                            toastr.error(errorMsg, "Failed");
                        });
                    } else {
                        alert("Please select at least on menu")
                    }
                } else {
                    alert("Please select at least on outlet")
                }
            }
        };

        $scope.exportToXLS = function (value) {
            $timeout(function () {
                Excel.tableToExcel("#roleTableToExport", 'Role-List', 'Role_List_Report.xls');
            }, 1000); // trigger download
        };

        $scope.outletAccessIdFilter = function (outletAccessIdArr) {
            if (angular.isDefined($scope.outletListsData)) {
                $scope.tempOutletAccessObj = {};
                var tempOutletAccessArr = [];
                for (var i = 0; i < outletAccessIdArr.length; i++) {
                    var outletAccessArr = $filter('filter')($scope.outletListsData, {id: outletAccessIdArr[i]});
                    $scope.tempOutletAccessObj = outletAccessArr[0];
                    if (angular.isDefined($scope.tempOutletAccessObj)) {
                        tempOutletAccessArr.push($scope.tempOutletAccessObj.outletDesc);
                    }
                }
                return tempOutletAccessArr.toString();
            }
        };
        /* "LoadSettings": function (SettingsData) {
         return SettingsData.fetch();
         },*/
    }

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('settings', {
                url: '/settings',
                controller: SettingsCtrl,
                template: '<ui-view></ui-view>',
                abstract: true,
                title: 'SETTINGS',
                data: {
                    menuId: 6
                },
                sidebarMeta: {
                    icon: 'fa fa-gear ',
                    order: 6
                },
                resolve: {
                    "LoadOutlets": function (OutletsData) {
                        return OutletsData.load();
                    },
                    "LoadRoles": function (RolesData) {
                        return RolesData.load();
                    }
                }
            })
            .state('settings.general', {
                parent: 'settings',
                url: '/generalSettings',
                templateUrl: 'app/pages/settings/general.settings.html',
                controller: SettingsCtrl,
                title: 'GENERAL',
                data: {
                    menuId: 7
                },
                sidebarMeta: {
                    icon: 'fa fa-sign-in',
                    order: 1
                }
            })
            .state('settings.smsGateway', {
                parent: 'settings',
                url: '/smsGatewaySettings',
                templateUrl: 'app/pages/settings/smsGateway.html',
                controller: SettingsCtrl,
                data: {
                    menuId: 8
                },
                title: 'SMS GATEWAY',
                sidebarMeta: {
                    icon: 'fa fa-wrench fa-lg',
                    order: 2
                }
            })
            .state('settings.createRole', {
                parent: 'settings',
                url: '/createRole',
                templateUrl: 'app/pages/settings/create.role.html',
                controller: SettingsCtrl,
                data: {
                    menuId: 9
                },
                title: 'ROLES',
                sidebarMeta: {
                    icon: 'fa fa-wrench fa-lg',
                    order: 3
                }
            })
    }
})();
