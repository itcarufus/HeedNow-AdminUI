/**
 * Created by LENOVO on 13-12-2016.
 */
/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('UApps.pages.templates', [])
        .config(routeConfig).controller('TemplatesCtrl', TemplatesCtrl);

    function TemplatesCtrl($location, editableThemes, editableOptions, TemplatesData, toastr, $uibModal, Template, $scope, AuthenticationService) {

        $scope.templateListsData = $scope.templateListsMasterData = TemplatesData.getList();
        $scope.templatePageSize = 10;
        $scope.statusOptions = [
            {id: 'A', text: 'Active'},
            {id: 'I', text: 'InActive'}
        ];

        var editModalBox;
        $scope.hasAddRights = AuthenticationService.userHasMenuAccess(16);
        $scope.hasViewRights = AuthenticationService.userHasMenuAccess(19);
        $scope.hasEditRights = AuthenticationService.userHasMenuAccess(13);
        $scope.hasTemplateEditRights = AuthenticationService.userHasMenuAccess(29);

        $scope.gotoCreateTemplate = function (item) {
            editModalBox = $uibModal.open({
                animation: true,
                templateUrl: 'app/pages/templates/createTemplate.html',
                size: 'md',
                backdrop: 'static',
                keyboard: false,
                scope: $scope
            });
        };
        var editTemplateModalBox;
        $scope.editTemplateBox = function (item) {
            $scope.editTemplate.info=item;
            editTemplateModalBox = $uibModal.open({
                animation: true,
                templateUrl: 'app/pages/templates/editTemplate.html',
                size: 'md',
                backdrop: 'static',
                keyboard: false,
                scope: $scope
            });
        };
        var showOutletsBox;
        $scope.showOutlets = function (outlets) {
            $scope.outlets=outlets;
            showOutletsBox = $uibModal.open({
                animation: true,
                templateUrl: 'app/pages/templates/linkedOutlets.html',
                size: 'md',
                backdrop: 'static',
                keyboard: false,
                scope: $scope
            });
        };


        $scope.goToTemplateListPage = function () {
            $location.path("/templates");
        };

        $scope.goToAssignQuestionPage = function (item) {
            $location.path("/assignQuestions/" + item.templateId);
        };
        $scope.newTemplate = {
            form: {},
            info: Template.newObject()
        };
        $scope.editTemplate = {
            form: {},
            info: Template.newObject()
        };

        $scope.createTemplate = function (isValid) {
            if (isValid) {
                TemplatesData.create($scope.newTemplate.info).then(function (newdata) {
                    toastr.success("Template created successfully!", "Success");
                    $scope.newTemplate.form.$setPristine();
                    $scope.newTemplate.info = Template.newObject();
                    $scope.templateListsMasterData = newdata;
                    $scope.templateListsData = [].concat($scope.templateListsMasterData);
                    editModalBox.close();
                }, function (errorMsg) {
                    toastr.error(errorMsg, "Failed");
                });
            }
        };

        $scope.deleteTemplate = function (id) {
            TemplatesData.delete(id).then(function () {
                toastr.success("Template deleted successfully!", "Success");
                $scope.templateListsMasterData = TemplatesData.getList();
                $scope.templateListsData = [].concat($scope.templateListsMasterData);
            }, function (errorMsg) {
                toastr.error(errorMsg, "Failed");
            });
        };

     /*   $scope.editTemplate = {
            form: {},
            info: Template.newObject()
        };
*/
        $scope.updateTemplate = function (isValid) {
            if(isValid){
                TemplatesData.update(Template.updateTemplateObject($scope.editTemplate.info)).then(function () {
                    toastr.success("Template updated successfully!", "Success");
                    editTemplateModalBox.close();
                    $scope.editTemplate.form.$setPristine();
                    $scope.editTemplate.info=Template.newObject();
                }, function (errorMsg) {
                    toastr.error(errorMsg, "Failed");
                });
            }
        };

        editableOptions.theme = 'bs3';
        editableThemes['bs3'].submitTpl = '<button type="submit" class="btn btn-primary btn-with-icon"><i class="fa fa-check fa-lg" aria-hidden="true"></i></button>';
        editableThemes['bs3'].cancelTpl = '<button type="button" ng-click="$form.$cancel()" class="btn btn-default btn-with-icon"><i class="fa fa-close fa-lg"></i></button>';

    }


    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('templates', {
                url: '/templates',
                templateUrl: 'app/pages/templates/templates.list.html',
                controller: TemplatesCtrl,
                title: 'TEMPLATES',
                data:{
                    menuId: 2
                },
                sidebarMeta: {
                    icon: 'fa fa-list-alt',
                    order: 2
                },
                resolve: {
                    "LoadTemplates": function (TemplatesData) {
                        return TemplatesData.load();
                    }
                }
            })
    }

})();
