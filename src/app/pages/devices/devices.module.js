(function () {
    'use strict';

    angular.module('UApps.pages.devices', [])
        .config(routeConfig).controller('devicesCtrl', devicesCtrl)
        .filter('filterWithinOneDay', filterWithinOneDay)
        .filter('filterPastOneDay', filterPastOneDay)
        .filter('filterWithoutFeed', filterWithoutFeed)
        .filter('androidVersion', androidVersion);
    /*        .factory('DevicesExcel', function ($window, Base64) {
     var uri = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8;base64,',
     template = '<html xmlns:v="urn:schemas-microsoft-com:vml"  xmlns:o="urn:schemas-microsoft-com:office:office"  xmlns:x="urn:schemas-microsoft-com:office:excel"  xmlns="http://www.w3.org/TR/REC-html40">' +
     '<head><style>font-family{font-size:10.0pt; font-family:Verdana, sans-serif;  mso-font-charset:0;} th, td {border:.1pt solid black;}</style>' +
     '<!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
     base64 = function (s) {
     return $window.btoa(unescape(encodeURIComponent(s)));
     },
     format = function (s, c) {
     return s.replace(/{(\w+)}/g, function (m, p) {
     return c[p];
     })
     };
     return {
     tableToExcel: function (tableId, worksheetName) {
     /!*var table = $(tableId),
     ctx = {worksheet: worksheetName, table: table.html()},
     href = uri + base64(format(template, ctx));
     return href;*!/
     var table = $(tableId),
     template = '<html xmlns:v="urn:schemas-microsoft-com:vml"  xmlns:o="urn:schemas-microsoft-com:office:office"  xmlns:x="urn:schemas-microsoft-com:office:excel"  xmlns="http://www.w3.org/TR/REC-html40">' +
     '<head><style>font-family{font-size:10.0pt; font-family:Verdana, sans-serif;  mso-font-charset:0;} th, td {border:.1pt solid black;}</style>' +
     '<!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>DevicesData</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>' + table.html() + '</table></body></html>';
     var blob = new Blob([template], {
     type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
     });
     saveAs(blob, "Devices-Raw-Data.xls");

     }
     };
     });*/

    function androidVersion() {
        return function (value) {
            if (value != "") {
                return value.split("/")[2].split(":")[1];
            }
            return "";
        };
    }

    function filterWithinOneDay() {
        return function (items) {
            var dt = new Date();
            var df = new Date();
            df.setDate(dt.getDate() - 1);
            var result = [];
            for (var i = 0; i < items.length; i++) {
                var tf = new Date(items[i].feedbackDate);
                if (tf > df && tf < dt) {
                    result.push(items[i]);
                }
            }
            return result;
        };
    }

    function filterPastOneDay() {
        return function (items) {
            var dt = new Date();
            var df = new Date();
            df.setDate(dt.getDate() - 1);
            var result = [];
            for (var i = 0; i < items.length; i++) {
                var tf = new Date(items[i].feedbackDate);
                if (tf < df) {
                    result.push(items[i]);
                }
            }
            return result;
        };
    }

    function filterWithoutFeed() {
        return function (items) {
            var result = [];
            for (var i = 0; i < items.length; i++) {
                if (items[i].feedbackDate == "") {
                    result.push(items[i]);
                }
            }
            return result;
        };
    }



    function devicesCtrl(editableThemes,$rootScope, Excel, AuthenticationService, editableOptions, DevicesData, $uibModal, toastr, $scope, $timeout) {

        $scope.uniqueStores = [];
        //$scope.deviceListsData = $scope.deviceListsMasterData = ;
        var devicesRawList = DevicesData.getList();
        var uniqueDeviceList = [];
        var unqIds = [];

        $scope.hasEditRights = AuthenticationService.userHasMenuAccess(25);


        $scope.exportToXLS = function (value) {
            $timeout(function () {
                Excel.tableToExcel("#devicesTableToExport", 'DevicesData', 'Device-Data-Report.xls');
            }, 1000); // trigger download
        };
        for (var i = 0; i < devicesRawList.length; i++) {

            var obj = devicesRawList[i];
            if ($scope.uniqueStores.indexOf(obj.storeId.toUpperCase()) === -1) {
                $scope.uniqueStores.push(obj.storeId.toUpperCase());
            }
            var installationObj = {
                installationId: obj.installationId,
                installationDate: obj.installationDate,
                lastFeedbackDate: obj.feedbackDate
            }
            if (unqIds.indexOf(obj.androidDeviceId) === -1) {
                unqIds.push(obj.androidDeviceId);
                uniqueDeviceList.push(obj);
                obj.installations = [];
                obj.installations.push(installationObj)
            } else {
                var exisObj = findDeviceObjById(uniqueDeviceList, obj.androidDeviceId);
                exisObj.installations.push(installationObj);
                exisObj.feedbackDate = obj.feedbackDate
            }
        }
        $scope.deviceListsData = $scope.deviceListsMasterData = uniqueDeviceList;
        function findDeviceObjById(arr, id) {
            for (var j = 0; j < arr.length; j++) {
                if (arr[j].androidDeviceId == id) {
                    return arr[j];
                }
            }
            return null;
        }

        $scope.devicePageSize = 10;

        $scope.statusOptions = [
            {id: 'A', text: 'Active'},
            {id: 'I', text: 'InActive'}
        ];
        $scope.addNewDevice = {
            form: {},
            info: {
                serialNo: "",
                model: "",
                androidVersion: "",
                installationDate: ""
            }
        };

        $scope.isValidSerial = function isValidSerial(val) {
            var regex = /[A-z0-9]/;
            return regex.test(val);
        };


        $scope.addDevice = function (isValid) {
            if (isValid) {
                /*  if (checkMACAddress($scope.addNewDevice.info.macAdd)) {*/
                DevicesData.create($scope.addNewDevice.info).then(function (response) {
                    toastr.success("Device added successfully", "Success");
                    $scope.addNewDevice.form.$setPristine();
                    $scope.addNewDevice.info = "";
                    DevicesData.load().then(function () {
                        $scope.deviceListsMasterData = DevicesData.getList();
                        $scope.deviceListsData = [].concat($scope.deviceListsMasterData);
                    });
                    addNewDevicePopUp.close();
                }, function (errorMsg) {
                    toastr.error(errorMsg, "Failed");
                });
                //}
            }
        };

        function checkMACAddress(value) {
            var macAddress = value;
            var macAddressRegExp = /^([0-9A-F]{2}:){5}[0-9A-F]{2}$/i;

            if (macAddress.length != 17) {
                alert('Mac Address is not the proper length.');
                return false;
            }

            if (macAddressRegExp.test(macAddress) == false) { //if match failed
                alert("Please enter a valid MAC Address.");
                return false;
            }

            return true;
        }


        $scope.updateDevice = function (item) {
            var clientId=$rootScope.globals.currentUser.clientId;
            DevicesData.update(item,clientId).then(function (response) {
                toastr.success("Device updated successfully", "Success");
            }, function (errorMsg) {
                toastr.error(errorMsg, "Failed");
            });
        };

        /*   $scope.create = function () {
         DevicesData.create($scope.addNewDevice.info).then(function (response) {
         toastr.success("Device added successfully");
         addNewDevicePopUp.close();
         }), function (errorMsg) {
         toastr.error(errorMsg, "Failed")
         }
         };*/


        var addNewDevicePopUp;
        $scope.goToAddNewDevice = function () {
            addNewDevicePopUp = $uibModal.open({
                animation: true,
                templateUrl: 'app/pages/devices/createDevice.html',
                size: 'md',
                backdrop: 'static',
                keyboard: false,
                scope: $scope
            });
        };
        $scope.deviceData = {};
        var installationHistoryPopup;
        $scope.showInstallationHistory = function (deviceItem) {
            $scope.deviceData = deviceItem;
            installationHistoryPopup = $uibModal.open({
                animation: true,
                templateUrl: 'app/pages/devices/device.installations.html',
                size: 'md',
                backdrop: 'static',
                keyboard: false,
                scope: $scope
            });
        };


        $scope.devicesFeedWithinDay = function () {
            $uibModal.open({
                animation: true,
                templateUrl: 'app/pages/devices/devices.withinday.html',
                size: 'md',
                backdrop: 'static',
                keyboard: false,
                scope: $scope
            });
        };

        $scope.devicesFeedPastDay = function () {
            $uibModal.open({
                animation: true,
                templateUrl: 'app/pages/devices/devices.pastday.html',
                size: 'md',
                backdrop: 'static',
                keyboard: false,
                scope: $scope
            });
        };

        $scope.devicesWithoutFeed = function () {
            $uibModal.open({
                animation: true,
                templateUrl: 'app/pages/devices/devices.withoutfeed.html',
                size: 'md',
                backdrop: 'static',
                keyboard: false,
                scope: $scope
            });
        };

        editableOptions.theme = 'bs3';
        editableThemes['bs3'].submitTpl = '<button type="submit" class="btn btn-primary btn-with-icon"><i class="fa fa-check fa-lg" aria-hidden="true"></i></button>';
        editableThemes['bs3'].cancelTpl = '<button type="button" ng-click="$form.$cancel()" class="btn btn-default btn-with-icon"><i class="fa fa-close fa-lg"></i></button>';

    }


    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('devices', {
                url: '/devices',
                controller: devicesCtrl,
                templateUrl: 'app/pages/devices/devices.list.html',
                title: 'DEVICES',
                data: {
                    menuId: 5
                },
                sidebarMeta: {
                    icon: 'fa fa-tablet',
                    order: 5
                },
                resolve: {
                    "LoadDevices": function (DevicesData) {
                        return DevicesData.load();
                    }
                }
            })
    }
})();
