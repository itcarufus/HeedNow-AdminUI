/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('UApps.pages.users', [])
        .config(routeConfig).controller('usersCtrl', usersCtrl);

    function usersCtrl($location, OutletsData,ValidationServices, $timeout,Excel, $filter, RolesData, UsersData, User, toastr, $scope, AuthenticationService, questionAnswerTypes) {

        $scope.usersData = $scope.usersMasterData = UsersData.getList();
        $scope.outletListsData = $scope.outletListsMasterData = OutletsData.getList();
        $scope.roleListData = $scope.roleListMasterData = RolesData.getList();
        $scope.userPageSize = 10;
        $scope.statusOptions = [
            {id: 'A', text: 'Active'},
            {id: 'I', text: 'InActive'}
        ];
        $scope.hasAddRights = AuthenticationService.userHasMenuAccess(14);
        $scope.hasEditRights = AuthenticationService.userHasMenuAccess(15);
        $scope.goToAddUserPage = function () {
            $location.path("/createUser");
        };
        $scope.goToEditUserPage = function (item) {
            $location.path("/editUser/" + item.id);
        };

        $scope.isUsername = ValidationServices.isUsername;
        $scope.isName = ValidationServices.isName;
        $scope.isPassword = ValidationServices.isPassword;
        $scope.isUnselected = ValidationServices.isUnselected;

        $scope.selected = {
            role: ""
        };

        $scope.exportToXLS = function (value) {
            $timeout(function () {
                Excel.tableToExcel("#userTableToExport", 'UserData', 'User-Data-Report.xls');
            }, 1000); // trigger download
        };


        $scope.newUser = {
            form: {},
            info: {
                name: "",
                userName: "",
                email: "",
                password: "",
                roleId: "",
                notifyEmail: ""
            }
        };
        $scope.notifyEmail = {
            isChecked: false
        };

        $scope.createUser = function (isValid) {
            if (isValid) {
                if ($scope.selected.role.length != 0) {
                    if ($scope.notifyEmail.isChecked) {
                        $scope.newUser.info.notifyEmail = 1;
                    } else {
                        $scope.newUser.info.notifyEmail = 0;
                    }
                    $scope.newUser.info.roleId = $scope.selected.role.roleId;
                    UsersData.create($scope.newUser.info).then(function () {
                        toastr.success("User created successfully!", "Success");
                        $scope.newUser.form.$setPristine();
                        $scope.newUser.info = "";
                        $location.path("/users");
                    }, function (errorMsg) {
                        toastr.error(errorMsg, "Failed");
                    });
                } else {
                    alert("Please select at least one role")
                }
            }
        };

        $scope.fetchRoleNameFromId = function (roleId) {
            for (var i in $scope.roleListData) {
                if ($scope.roleListData[i].roleId == roleId)
                    return $scope.roleListData[i].name;
            }
        };
        $scope.outletAccessIdFilter = function (outletAccessIdArr) {
            if (angular.isDefined($scope.outletListsData)) {
                $scope.tempOutletAccessObj = {};
                var tempOutletAccessArr = [];
                for (var i = 0; i < outletAccessIdArr.length; i++) {
                    var outletAccessArr = $filter('filter')($scope.outletListsData, {id: outletAccessIdArr[i]});
                    $scope.tempOutletAccessObj = outletAccessArr[0];
                    if (angular.isDefined($scope.tempOutletAccessObj)) {
                        tempOutletAccessArr.push($scope.tempOutletAccessObj.outletDesc);
                    }
                }
                return tempOutletAccessArr.toString();
            }
        };

    }


    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider

            .state('users', {
                url: '/users',
                controller: usersCtrl,
                templateUrl: 'app/pages/users/users.list.html',
                title: 'USERS',
                data: {
                    menuId: 10
                },
                sidebarMeta: {
                    icon: 'fa fa-user',
                    order: 7
                },
                resolve: {
                    "LoadUsers": function (UsersData) {
                        return UsersData.load();
                    },
                    "LoadRoles": function (RolesData) {
                        return RolesData.load();
                    },
                    "LoadOutlets": function (OutletsData) {
                        return OutletsData.load();
                    }
                }
            })
            .state('createUser', {
                url: '/createUser',
                templateUrl: 'app/pages/users/createUser.html',
                controller: usersCtrl,
                title: 'CREATE USER',
                data: {
                    menuId: 14
                },
                resolve: {
                    "LoadRoles": function (RolesData) {
                        return RolesData.load();
                    }
                }
            })


    }

})();
