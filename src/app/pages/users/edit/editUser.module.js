/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('UApps.pages.questions')
        .config(routeConfig).controller('editUserCtrl', editUserCtrl);

    function editUserCtrl($stateParams,$rootScope, ValidationServices, $location, User, RolesData, UsersData, $filter, toastr, $scope, symbolTypes) {

        $scope.roleListData = $scope.roleListMasterData = RolesData.getList();
        getQuestionInfo();

        $scope.editUser = {
            form: {},
            info: {
                name: "",
                userName: "",
                email: "",
                password: "",
                role: "",
                status: "",
                notifyEmail: ""
            }
        };
        $scope.notifyEmail = {
            isChecked: false
        };
        $scope.isPassword = ValidationServices.isPassword;

        $scope.isName = ValidationServices.isName;


        function getQuestionInfo() {
            UsersData.userInfo($stateParams.userId).then(function (response) {
                $scope.userInfo = response;
                $scope.editUser.info = $scope.userInfo;
                $scope.editUser.info.roleId = $scope.userInfo.roleId;
                var selectedRole = $filter('filter')($scope.roleListData, {roleId: $scope.userInfo.roleId});
                $scope.selected.role = selectedRole[0];
                if ($scope.userInfo.notifyEmail == 1) {
                    $scope.notifyEmail.isChecked = true;
                } else {
                    $scope.notifyEmail.isChecked = false;
                }
            });
        }

        $scope.selected = {
            role: {}
        };
        $scope.statusOptions = [
            {id: 'A', text: 'Active'},
            {id: 'I', text: 'InActive'}
        ];
        $scope.updateUser = function (isValid) {
            if (isValid) {
                if ($scope.selected.role.length != 0) {
                    if ($scope.notifyEmail.isChecked) {
                        $scope.editUser.info.notifyEmail = 1;
                    } else {
                        $scope.editUser.info.notifyEmail = 0;
                    }
                    $scope.editUser.info.role = $scope.selected.role.roleId;
                    UsersData.update(User.updateObject($scope.editUser.info)).then(function () {
                        toastr.success("User updated successfully!", "Success");
                        $location.path("/users");
                    }, function (errorMsg) {
                        toastr.error(errorMsg, "Failed");
                    });
                } else {
                    alert("Please select at least one role")
                }
            }
        };
        $scope.resetPasswordData = {
            form: {},
            info: {
                id: $stateParams.userId,
                newPassword: ""
            }
        };

        $scope.resetPassword = function (isValid) {
            if (isValid) {
                UsersData.resetPassword($scope.resetPasswordData.info).then(function () {
                    toastr.success("Password reset successfully", "Success");
                    $scope.resetPasswordData.form.$setPristine();
                    $scope.resetPasswordData.info.newPassword = "";
                }, function (errorMsg) {
                    toastr.error(errorMsg, "Failed");
                });
            }
        };

    }


    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('editUser', {
                url: '/editUser/:userId',
                templateUrl: 'app/pages/users/edit/editUser.html',
                controller: editUserCtrl,
                title: 'Edit User',
                data: {
                    menuId: 15
                },
                resolve: {
                    "LoadRoles": function (RolesData) {
                        return RolesData.load();
                    }
                }
            })

    }

})();
