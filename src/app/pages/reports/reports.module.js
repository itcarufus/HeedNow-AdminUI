(function () {
    'use strict';

    angular.module('UApps.pages.reports', [])
        .config(routeConfig).controller('ReportsCtrl', ReportsCtrl)
        .filter('filterByValue', filterByValue);

        function filterByValue() {
        return function (items, value) {
            var outlets = [];
            if (value == 4) {
                for (var i = 0; i < items.length; i++) {
                    if (items[i].monthlyUnAddressedCount != 0) {
                        outlets.push(items[i]);
                    }
                }
                return outlets;
            } else if (value == 3) {
                for (var i = 0; i < items.length; i++) {
                    if (items[i].dailyUnAddressedCount != 0) {
                        outlets.push(items[i]);
                    }
                }
                return outlets;
            } else if (value == 2) {
                for (var i = 0; i < items.length; i++) {
                    if (items[i].monthlyNegativeCount != 0) {
                        outlets.push(items[i]);
                    }
                }
                return outlets;
            } else if (value == 1) {
                for (var i = 0; i < items.length; i++) {
                    if (items[i].dailyNegativeCount != 0) {
                        outlets.push(items[i]);
                    }
                }
                return outlets;
            }

        };
    }

    function ReportsCtrl($scope, $rootScope, $location, ReportData, OutletsData, $timeout, FeedbackData, toastr, AuthenticationService, Excel, $filter) {

        $scope.outletListsData = $scope.outletListsMasterData = OutletsData.getList();
        $scope.reportPageSize = 15;
        $scope.dailyReportData = "";
        $scope.tempOutletReportData="";
        /*$scope.generateFeedback = function(){
         var httpService = new HttpService("feedback");
         httpService.post("list").then(function(response){
         $window.open("../feedback/Feedbacks.xls");
         });
         };*/


        $scope.hasFullPermission = AuthenticationService.userHasMenuAccess(26);
        $scope.hasDownloadRights = AuthenticationService.userHasMenuAccess(27);
        $scope.isLoading = false;
        $scope.selected = {
            outlet: {}
        };
        $scope.excelHeaders = {
            questions: [],
            options: []
        };

        $scope.negativeReportPage = function () {
            $location.path("/negativeReport");
        };

        $scope.isValidTable = function isValidTable(val) {
            var regex = /^[-0-9, ]+$/;
            return regex.test(val);

        };
        var userId = $rootScope.globals.currentUser.userId;
        $scope.genReport = {
            form: {},
            info: {
                userId: userId,
                fromDate: "",
                toDate: "",
                tableNo: ""
            }
        };
        $scope.ifFormDateNotSelected = true;
        $scope.open1 = function () {
            if ($scope.genReport.info.fromDate.getMonth) {
                $scope.genReport.info.toDate = $scope.genReport.info.fromDate;
                setFirstAndLastDate($scope.genReport.info.fromDate);
            }
            $scope.popup1.opened = true;
        };
        $scope.popup1 = {
            opened: false

        };
        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };
        $scope.enableDisablePopUp=function(){
            if($scope.genReport.info.fromDate!="" && angular.isDefined($scope.genReport.info.fromDate)){
                $scope.genReport.info.toDate="";
                $scope.ifFormDateNotSelected = false;
            }else {
                $scope.genReport.info.toDate="";
                $scope.ifFormDateNotSelected = true;
            }
        };
        $scope.popup2 = {
            opened: false
        };
        $scope.format = 'dd-MMMM-yyyy';
        setFirstAndLastDate(new Date());
        function setFirstAndLastDate(dt) {
            var date = dt, y = date.getFullYear(), m = date.getMonth();
            var firstDay = new Date(y, m, 1);
            var lastDay = new Date(y, m + 1, 0);
            $scope.minDate = firstDay;
            $scope.maxDate = lastDay;
        }
/*
        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };
        $scope.popup1 = {
            opened: false
        };
        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };
        $scope.popup2 = {
            opened: false
        };*/
        $scope.reportDataLoaded = false;

        getDailyReportByUserId();
        function getDailyReportByUserId() {
            ReportData.dailyReportByUserId().then(function (response) {
                $scope.reportDataLoaded = true;
                $scope.dailyReportData = response;
                $scope.tempOutletReportData = response.outlets;
                $scope.outletWiseReportMasterData = $scope.dailyReportData.outlets;
                $scope.outletWiseReportData = [].concat($scope.outletWiseReportMasterData);
            });
        }

        $scope.generateReport = function (isValid) {
            if (isValid) {
                $scope.genReport.info.outletId = $scope.selected.outlet.id;
                var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
                var firstDate = new Date($scope.genReport.info.fromDate);
                var secondDate = new Date($scope.genReport.info.toDate);
                var diffDays = Math.round((secondDate.getTime() - firstDate.getTime()) / oneDay);
                var outletIds = [];
                if (diffDays <= 30 && diffDays >= 0) {
                    $scope.isLoading = true;
                    for (var i = 0; i < $scope.selected.outlet.length; i++) {
                        outletIds.push($scope.selected.outlet[i].id);
                    }
                    ReportData.genNegativeFeedbackReport($scope.genReport.info, outletIds).then(function (response) {
                        $scope.reportData = response;
                        $timeout(function () {
                            $scope.isLoading = false;
                            Excel.tableToExcel("#tableToExport", 'NegativeReport', 'Negative-Feedback-Report.xls');
                        }, 1000);
                    }, function (errorMsg) {
                        $scope.isLoading = false;
                        toastr.error(errorMsg, "No Data");
                    });
                }

            }
        };


        $scope.filterApplied=false;
        $scope.filterReport = function (value) {
            $scope.filterApplied=false;
            if (value == 1) {
                var tempData = $filter('filterByValue')($scope.tempOutletReportData, 1);
                $scope.outletWiseReportMasterData = tempData;
                $scope.outletWiseReportData = [].concat($scope.outletWiseReportMasterData);
                $scope.filterTitle=" Negative Feedback ( Daily )";
                $scope.filterApplied=true;
            } else if (value == 2) {
                var tempData = $filter('filterByValue')($scope.tempOutletReportData, 2);
                $scope.outletWiseReportMasterData = tempData;
                $scope.outletWiseReportData = [].concat($scope.outletWiseReportMasterData);
                $scope.filterTitle=" Negative Feedback ( MDT )";
                $scope.filterApplied=true;
            } else if (value == 3) {
                var tempData = $filter('filterByValue')($scope.tempOutletReportData, 3);
                $scope.outletWiseReportMasterData = tempData;
                $scope.outletWiseReportData = [].concat($scope.outletWiseReportMasterData);
                $scope.filterTitle=" Unaddressed Feedback ( Daily )";
                $scope.filterApplied=true;
            } else if (value == 4) {
                var tempData = $filter('filterByValue')($scope.tempOutletReportData, 4);
                $scope.outletWiseReportMasterData = tempData;
                $scope.outletWiseReportData = [].concat($scope.outletWiseReportMasterData);
                $scope.filterTitle=" Unaddressed Feedback ( MDT )";
                $scope.filterApplied=true;
            }
        };

        $scope.removeAllFilter=function(){
            $scope.outletWiseReportMasterData = $scope.tempOutletReportData;
            $scope.outletWiseReportData = [].concat($scope.outletWiseReportMasterData);
            $scope.filterApplied=false;
        }
    }


    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('reports', {
                url: '/reports',
                controller: ReportsCtrl,
                templateUrl: 'app/pages/reports/reports.html',
                title: 'REPORTS',
                data: {
                    menuId: 24
                },
                sidebarMeta: {
                    icon: 'fa fa-line-chart',
                    order: 8
                },
                resolve: {
                    "LoadOutlets": function (OutletsData) {
                        return OutletsData.load();
                    }
                }
            })
            .state('negativeReport', {
                url: '/negativeReport',
                templateUrl: 'app/pages/reports/negative-report.html',
                controller: ReportsCtrl,
                title: 'NEGATIVE REPORT',
                data: {
                    menuId: 27
                }
            })

    }
})();
