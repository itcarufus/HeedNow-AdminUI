(function () {
    'use strict';

    angular.module('UApps.pages.feedbackView', [])
        .config(routeConfig).controller('feedbackViewCtrl', feedbackViewCtrl)

    function feedbackViewCtrl($scope,$rootScope, ReportData, $stateParams, $location, FeedbackData, $timeout, toastr, $filter, QuestionsData) {

/*
        $scope.enter = {
            captcha: ""
        };
        var feedbackId = $stateParams.Id;
        $scope.feedbackTrack = false;
        //createTrack();
        $scope.createTrack = function () {
            if ($scope.enter.captcha == $scope.captchaStr) {
                $scope.$location = $location;
                $scope.url = $scope.$location.url();
                $scope.loc1 = $scope.$location.search().sp;
                if ($location.url().indexOf('SC') > -1) {

                    ReportData.createFeedbackTracking($stateParams.Id).then(function (response) {
                        /!*  $scope.loc= $location.url().split('=')[1];
                         $scope.loc = $scope.loc.split("#")[0]*!/
                        $scope.feedbackTrack = true;
                    });
                }
            } else {
                $scope.WrongCaptcha = true;
            }

        };
        $scope.captcha = "";
        $scope.WrongCaptcha = false;
        Captcha()
        function Captcha() {
            var alpha = new Array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
            var i;
            for (i = 0; i < 6; i++) {
                var a = alpha[Math.floor(Math.random() * alpha.length)];
                var b = alpha[Math.floor(Math.random() * alpha.length)];
                var c = alpha[Math.floor(Math.random() * alpha.length)];
                var d = alpha[Math.floor(Math.random() * alpha.length)];
                var e = alpha[Math.floor(Math.random() * alpha.length)];
                var f = alpha[Math.floor(Math.random() * alpha.length)];
                var g = alpha[Math.floor(Math.random() * alpha.length)];
            }
            var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e + ' ' + f + ' ' + g;
            var codeStr = a + b + c + d + e + f + g;

            $scope.captcha = code;
            $scope.captchaStr = codeStr;

        }
*/


        var feedbackId = $stateParams.Id;
        createTrack();
        function createTrack() {
            $scope.$location = $location;
            $scope.url = $scope.$location.url();
            $scope.loc1 = $scope.$location.search().sp;
            if ($location.url().indexOf('SC') > -1) {
                ReportData.createFeedbackTracking($stateParams.Id).then(function (response) {
                    /*  $scope.loc= $location.url().split('=')[1];
                     $scope.loc = $scope.loc.split("#")[0]*/
                });
            }
        }

        getFeedbackInfo();
        function getFeedbackInfo() {
            FeedbackData.feedbackDetails(feedbackId).then(function (response) {
                $scope.feedbackInfo = response;
                var oldFeedbackList = response.feedbacks;
                var uniqueQuesid = [];
                var newFeedbackList = [];
                for (var j = 0; j < oldFeedbackList.length; j++) {
                    var obj = oldFeedbackList[j];
                    var extraObj = {
                        "questionId": obj.questionId,
                        "answerText": obj.answerText,
                        "answerValue": obj.answerDesc,
                        "answerId": obj.answerId,
                        "rating": obj.rating,
                        "questionType": obj.questionType,
                        "questionDesc": obj.questionDesc,
                        "answerDesc": obj.answerDesc,
                        "isNegative": obj.isNegative
                    };

                    if (obj.questionType != "5") {
                        if (obj.questionType == "2" || obj.questionType == "3") {
                            extraObj.answerValue = $filter('ratingFilter')(obj.rating);
                        }
                        if (obj.questionType == "4") {
                            extraObj.answerValue = obj.answerText;
                        }
                        uniqueQuesid.push(obj.questionId);
                        newFeedbackList.push(extraObj);
                    } else {
                        //MultiSelect Ques
                        if (uniqueQuesid.indexOf(obj.questionId) === -1) {
                            uniqueQuesid.push(obj.questionId);
                            newFeedbackList.push(extraObj);
                        } else {
                            var existingObj = $filter('filter')(newFeedbackList, {questionId: obj.questionId});
                            existingObj.answerValue += ", " + obj.answerDesc;
                        }
                    }
                }
                var finalList = [];
                uniqueQuesid = [];
                for (var j = 0; j < newFeedbackList.length; j++) {
                    var obj = newFeedbackList[j];
                    if (uniqueQuesid.indexOf(obj.questionId) === -1) {
                        var quesObj = {
                            "questionId": obj.questionId,
                            "questionType": obj.questionType,
                            "questionDesc": obj.questionDesc,
                            "isNegative": obj.isNegative,
                            options: []
                        };
                        uniqueQuesid.push(obj.questionId);
                        quesObj.options.push(obj);
                        finalList.push(quesObj);
                    } else {
                        var existingQuesObj = $filter('filter')(finalList, {questionId: obj.questionId});
                        existingQuesObj[0].options.push(obj);
                    }
                }
                $scope.feedbackInfo.feedbacks = finalList;

            /*    for (var i = 0; i < finalList.length; i++) {//to check isnegative by calling api continue
                    QuestionsData.questionInfo(finalList[i].questionId).then(function (response) {
                        var existingQuesObj = $filter('filter')(finalList, {questionId: response.id})[0];
                        for (var j = 0; j < response.options.length; j++) {
                            var ansObj = $filter('filter')(existingQuesObj.options, {answerId: response.options[j].answer_id})[0];
                            var threshold = response.options[j].threshold;
                            if (threshold != null && threshold != "") {
                                if (existingQuesObj.questionType == "2" || existingQuesObj.questionType == "3") {
                                    var gap = response.options[j].rating / response.options[j].weightage;
                                    var actualValue = ansObj.rating / gap;
                                    if (actualValue <= parseInt(threshold)) {
                                        ansObj.isNegative = true;
                                        existingQuesObj.isNegative = true;
                                    }
                                } else if (existingQuesObj.questionType == "1" || existingQuesObj.questionType == "5" || existingQuesObj.questionType == "6") {
                                    if (threshold == "1") {
                                        ansObj.isNegative = true;
                                        existingQuesObj.isNegative = true;
                                    }
                                }
                            }


                        }
                    });
                }*/
            });
        }

    }


    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('feedbackView', {
                url: '/feedbackView/:Id',
                controller: feedbackViewCtrl,
                templateUrl: 'app/pages/feedbackView/feedback.View.html',
            })
    }
})();
