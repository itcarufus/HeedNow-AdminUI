(function () {
    'use strict';

    angular.module('UApps.pages.dashboard', [])
        .config(routeConfig).controller('DashboardCtrl', DashboardCtrl);
    function DashboardCtrl($timeout,ClientsData, AuthenticationService, $scope, TemplatesData, FeedbackData, $interval, $uibModal, $rootScope, $filter, ReportData, toastr, $location) {

        var clientId = $rootScope.globals.currentUser.clientId;
        $scope.templateListsData = TemplatesData.getList();
        var welcomeModal;

     /*   if ($rootScope.afterLogin) {
            showWelcome();
            getClientInfo();
        }*/
        function showWelcome() {
            $timeout(function () {
                welcomeModal = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/pages/dashboard/welcome.html',
                    size: 'md',
                    backdrop: 'static',
                    keyboard: false,
                    scope: $scope
                });
            }, 50);
            $rootScope.afterLogin = false;
        }

        function getClientInfo() {
            ClientsData.clientInfo(clientId).then(function (response) {
                $scope.clientInfo = response;
            })
        }

        $scope.dismissWelcome = function () {
            welcomeModal.close();
            $rootScope.afterLogin = false;
        };

        var uniqueNames = [];
        var uniqueObj = [];
        for (var i = 0; i < $scope.templateListsData.length; i++) {
            if (uniqueNames.indexOf($scope.templateListsData[i].templateId) === -1) {
                uniqueObj.push($scope.templateListsData[i]);
                uniqueNames.push($scope.templateListsData[i].templateId);
            }
        }

        $scope.templateListsData = uniqueObj;
        $scope.selected = {
            template: {},
            question: {},
            template2: {},
            question2: {}
        };
        $scope.graphCountDataLoaded = false;
        $scope.graphAvgDataLoaded = false;

        // var templateArr = $filter('filter')($scope.templateListsData, {templateId: 2});
        if($scope.templateListsData.length!=0){
            $scope.selected.template = $scope.templateListsData[0];
            onGetQuestionList($scope.selected.template.templateId);
            $scope.selected.template2 = $scope.templateListsData[0];
            onGetQuestion2List($scope.selected.template2.templateId);
            $rootScope.tempTemplate = $scope.selected.template2;//for storing data in cache
        }

        $scope.hasFullPermission = true;
        $scope.dashboardPageSize = 5;
        $scope.dashboardTableTitle = "Total Feedback";
        $scope.isValidTable = function isValidTable(val) {
            var regex = /^[-0-9, ]+$/;
            return regex.test(val);
        };
        var userId = $rootScope.globals.currentUser.userId;

        var outletAccess = $rootScope.globals.currentUser.outletAccess;
        $scope.reportPageSize = 10;

        var dt = new Date();
        var df = dt.setDate(dt.getDate() - 1);

        $scope.genReport = {
            info: {
                userId: userId,
                fromDate: df,
                toDate: new Date(),
                tableNo: "",
            }
        };

        $scope.gotofeedbackView = function (id) {
            $location.path("/feedbackView/" + id);
        };

        $scope.data = {
            loading: true
        };

        var outletIds = outletAccess != "" ? outletAccess.split(",") : [];

        ReportData.genDashBoardFeedbackReport($scope.genReport.info, outletIds, true).then(function (response) {

            $scope.reportFeedbackMasterData = response;
            $scope.reportFeedbackData = [].concat($scope.reportFeedbackMasterData);
            $scope.tempFeedback = $scope.reportFeedbackMasterData;
            $scope.feedbackCount = $scope.reportFeedbackData.length;
            var negativeFeedbackCount = $filter('filter')($scope.reportFeedbackData, {isNegative: 1});
            $scope.negativeFeedbackCount = negativeFeedbackCount.length;
            var seenFeedbackCount = $filter('filter')($scope.reportFeedbackData, {isAddressed: 1});
            $scope.seenFeedbackCount = seenFeedbackCount.length;
            if (angular.isDefined(seenFeedbackCount)) {
                var unseenFeedbackCount = $filter('filter')(negativeFeedbackCount, {isAddressed: 0});
                $scope.unseenTempData = unseenFeedbackCount;
                $scope.unseenFeedbackCount = unseenFeedbackCount.length;
            }
            $scope.data.loading = false
        }, function (errorMsg) {
            toastr.error(errorMsg, "No Data");
        });


        $scope.getQuestionRatingById = function (questionId) {
            ReportData.questionRatingById(questionId).then(function (response) {
                $scope.feedbackInfo = response;

            });
        };

        $scope.filterFeedback = function (value) {
            if (value == 1) {
                $scope.reportFeedbackMasterData = $scope.tempFeedback;
                $scope.reportFeedbackData = [].concat($scope.reportFeedbackMasterData);
                $scope.dashboardTableTitle = "Total Feedback";
            } else if (value == 2) {
                var tempData = $filter('filter')($scope.tempFeedback, {isNegative: 1});
                $scope.reportFeedbackMasterData = tempData;
                $scope.reportFeedbackData = [].concat($scope.reportFeedbackMasterData);
                $scope.dashboardTableTitle = "Negative Feedback";

            } else if (value == 3) {
                var tempData = $filter('filter')($scope.tempFeedback, {isNegative: 1, isAddressed: 1});
                $scope.reportFeedbackMasterData = tempData;
                $scope.reportFeedbackData = [].concat($scope.reportFeedbackMasterData);
                $scope.dashboardTableTitle = "Addressed Negative Feedback";
            } else if (value == 4) {
                var tempData = $filter('filter')($scope.tempFeedback, {isNegative: 1, isAddressed: 0});
                $scope.reportFeedbackMasterData = tempData;
                $scope.reportFeedbackData = [].concat($scope.reportFeedbackMasterData);
                $scope.dashboardTableTitle = "Unaddressed Negative Feedback";
            }
        };


        function getFeedbackInfo(feedbackId) {
            $scope.feedbackId = feedbackId;
            FeedbackData.feedbackDetails(feedbackId).then(function (response) {
                $scope.feedbackInfo = response;

                var oldFeedbackList = response.feedbacks;

                var uniqueQuesid = [];
                var newFeedbackList = [];
                for (var j = 0; j < oldFeedbackList.length; j++) {
                    var obj = oldFeedbackList[j];
                    var extraObj = {
                        "questionId": obj.questionId,
                        "answerText": obj.answerText,
                        "answerValue": obj.answerDesc,
                        "answerId": obj.answerId,
                        "rating": obj.rating,
                        "questionType": obj.questionType,
                        "questionDesc": obj.questionDesc,
                        "answerDesc": obj.answerDesc,
                        "isNegative": obj.isNegative
                    };

                    if (obj.questionType != "5") {
                        if (obj.questionType == "2" || obj.questionType == "3") {
                            extraObj.answerValue = $filter('ratingFilter')(obj.rating);
                        }
                        if (obj.questionType == "4") {
                            extraObj.answerValue = obj.answerText;
                        }
                        uniqueQuesid.push(obj.questionId);
                        newFeedbackList.push(extraObj);
                    } else {
                        //MultiSelect Ques
                        if (uniqueQuesid.indexOf(obj.questionId) === -1) {
                            uniqueQuesid.push(obj.questionId);
                            newFeedbackList.push(extraObj);
                        } else {
                            var existingObj = $filter('filter')(newFeedbackList, {questionId: obj.questionId});
                            existingObj.answerValue += ", " + obj.answerDesc;
                        }
                    }
                }
                var finalList = [];
                uniqueQuesid = [];
                for (var j = 0; j < newFeedbackList.length; j++) {
                    var obj = newFeedbackList[j];
                    if (uniqueQuesid.indexOf(obj.questionId) === -1) {
                        var quesObj = {
                            "questionId": obj.questionId,
                            "questionType": obj.questionType,
                            "questionDesc": obj.questionDesc,
                            "isNegative": obj.isNegative,
                            options: []
                        };
                        uniqueQuesid.push(obj.questionId);
                        quesObj.options.push(obj);
                        finalList.push(quesObj);
                    } else {
                        var existingQuesObj = $filter('filter')(finalList, {questionId: obj.questionId});
                        existingQuesObj[0].options.push(obj);
                    }
                }
                $scope.feedbackInfo.feedbacks = finalList;

            });
        };
        var viewFeedbackDetailReport;
        $scope.getFeedbackInfoBox = function (feedbackId) {
            getFeedbackInfo(feedbackId);
            viewFeedbackDetailReport = $uibModal.open({
                animation: true,
                templateUrl: 'app/pages/dashboard/view.feedbackDetails.html',
                size: 'lg',
                backdrop: 'static',
                keyboard: false,
                scope: $scope
            });
        };
        $scope.onTemplateChange = function () {
            $scope.graphAvgDataLoaded = false;
            onGetQuestionList($scope.selected.template.templateId);
        };
        $scope.onTemplate2Change = function () {
            $scope.graphCountDataLoaded = false;
            onGetQuestion2List($scope.selected.template2.templateId);
        };
        $scope.onQuestionChange = function () {
            $scope.graphAvgDataLoaded = false;
            generateQuestionGraph($scope.selected.question.id);
        };
        $scope.onQuestion2Change = function () {
            $scope.graphCountDataLoaded = false;
            generateQuestionRatingGraph($scope.selected.question2);
        };
        function onGetQuestionList(templateId) {
            TemplatesData.listQuestions(templateId).then(function (response) {
                $scope.templateQuestionsList = response;
                var tempQuestionObj = [];
                for (var i = 0; i < $scope.templateQuestionsList.length; i++) {
                    if ($scope.templateQuestionsList[i].questionType == 3) {
                        tempQuestionObj.push($scope.templateQuestionsList[i])
                    }
                }
                $scope.questionList = tempQuestionObj;
                $scope.selected.question = tempQuestionObj[0];
                generateQuestionGraph($scope.selected.question.id);
            });
        }

        function onGetQuestion2List(templateId) {
            TemplatesData.listQuestions(templateId).then(function (response) {
                $scope.templateQuestions2List = response;
                var tempQuestion2Obj = [];
                for (var i = 0; i < $scope.templateQuestions2List.length; i++) {
                    if ($scope.templateQuestions2List[i].questionType == 3) {
                        tempQuestion2Obj.push($scope.templateQuestions2List[i])
                    }
                }
                $scope.questionList2 = tempQuestion2Obj;
                $scope.selected.question2 = tempQuestion2Obj[0];
                generateQuestionRatingGraph($scope.selected.question2);

            });
        }

        $scope.pieOptions = {legend: {display: true}};
        $scope.ratings = ["Excellent", "Good", "Average", "Poor"];
        function generateQuestionGraph(questionId) {
            ReportData.questionAvgRatingById(questionId).then(function (response) {
                var feedbackReportByQuestion = response;
                var uniqueCategory = [];
                var feedbackCount = [];
                for (var i = 0; i < feedbackReportByQuestion.length; i++) {
                    if (uniqueCategory.indexOf(feedbackReportByQuestion[i].answerDesc) === -1) {
                        uniqueCategory.push(feedbackReportByQuestion[i].answerDesc)
                    }
                    feedbackCount.push(feedbackReportByQuestion[i].average);
                }
                $scope.questionCategory = uniqueCategory;
                $scope.feedbackReportByQuestion = feedbackCount
                $scope.graphAvgDataLoaded = true;
            })
        }

        function generateQuestionRatingGraph(question) {
            $scope.ratingLabel = ["Poor", "Average", "Good", "Very Good", "Excellent"];
            ReportData.questionRatingCountById(question.id).then(function (response) {
                var feedbackRatingByQuestion = response;
                var questionOption = [];
                var feedbackCount = [];
                for (var i = 0; i < question.options.length; i++) {
                    if (questionOption.indexOf(question.options[i].answerDesc) === -1) {
                        questionOption.push(question.options[i].answerDesc)
                    }
                }
                $scope.questionOption = questionOption;
                var subCategoryObj = [];
                var subCategoryArr = [];
                for (var j = 0; j < question.options.length; j++) {
                    for (var i = 0; i < feedbackRatingByQuestion.length; i++) {
                        if (feedbackRatingByQuestion[i].answerDesc == question.options[j].answerDesc) {
                            subCategoryObj.push(feedbackRatingByQuestion[i].count);
                            subCategoryObj.headLabel = feedbackRatingByQuestion[i].answerDesc
                        }
                    }
                    subCategoryArr.push(subCategoryObj);
                    subCategoryObj = [];
                }
                $scope.categoryWiseRatingData = subCategoryArr;
                $rootScope.tempGraphData = $scope.categoryWiseRatingData;
                $rootScope.tempQuestion = question;
                $scope.graphCountDataLoaded = true;
            });


        }

        $scope.options = {
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Ratings'
                    },
                    ticks: {
                        min: 0,
                        max: 100,
                        stepSize: 20
                    }
                }]
            }
        };
    }


    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('dashboard', {
                url: '/dashboard',
                controller: DashboardCtrl,
                templateUrl: 'app/pages/dashboard/dashboard.html',
                title: 'DASHBOARD',
                data: {
                    menuId: "D"
                },
                sidebarMeta: {
                    icon: 'fa fa-dashboard',
                    order: 1
                },
                resolve: {
                    "LoadTemplates": function (TemplatesData) {
                        return TemplatesData.load();
                    }
                }
            })
    }
})();
