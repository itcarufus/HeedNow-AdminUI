(function () {
    'use strict';

    angular.module('UApps.pages.outlets', [])
        .config(routeConfig).controller('outletsCtrl', outletsCtrl);

    function outletsCtrl($location, ClientsData,AuthenticationService, $rootScope, SettingsData, $filter, Template, editableThemes, editableOptions, TemplatesData, $uibModal, OutletsData, ValidationServices, toastr, $scope) {
        getSmsSetting();
        getGroupList(true);
        var clientId = $rootScope.globals.currentUser.clientId;
        var OutletObj = {
            userId: $rootScope.globals.currentUser.userId,
            outletId: ""
        };
        getClientInfo();
        function getClientInfo() {
            ClientsData.clientInfo(clientId).then(function (response) {
                $scope.clientInfo = response;
            })
        }

        $scope.gotoCreateOutlet = function () {
            $location.path("/createOutlet");
        };
        $scope.outletListsData = $scope.outletListsMasterData = OutletsData.getList(OutletObj);
        $scope.templateListsData = TemplatesData.getList();
        $scope.hasAddRights = AuthenticationService.userHasMenuAccess(28);
        $scope.hasAssignRights = AuthenticationService.userHasMenuAccess(17);
        var uniqueNames = [];
        var uniqueObj = [];

        for (var i = 0; i < $scope.templateListsData.length; i++) {

            if (uniqueNames.indexOf($scope.templateListsData[i].templateId) === -1) {
                uniqueObj.push($scope.templateListsData[i])
                uniqueNames.push($scope.templateListsData[i].templateId);
            }

        }
        $scope.templateListsData = uniqueObj;
        $scope.outletPageSize = 10;
        var bannerSettingBox;
        $scope.selected = {
            gateway: {}
        };
        $scope.newBannerSetting = {
            form: {},
            info: {
                bannerUrl: "",
                tableNoRange: "",
                mobileNoLength: "",
                pocEmail: "",
                pocName: "",
                pocMobile: "",
                mgrName: "",
                mgrEmail: "",
                mgrMobile: "",
                smsGatewayId: "",
            }
        };


        function getSmsSetting() {
            SettingsData.fetchSmsSetting().then(function (response) {
                $scope.smsSettingInfo = response;
            });
        };
        $scope.isValidTable = function isValidTable(val) {
            var regex = /^[-0-9, ]+$/;
            return regex.test(val);

        };

        $scope.isValidLength = function isName(val) {
            return !isNaN(val) && val != 0 && val <= 10;
        };

        $scope.isNumeric = function isNumeric(num) {
            return !isNaN(num);
        };

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };
        $scope.popup1 = {
            opened: false
        };
        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };
        $scope.popup2 = {
            opened: false
        };


        $scope.bannerSettingBox = function (item) {
            //$scope.outletData= item;
            getOutletInfo(item.id);
            bannerSettingBox = $uibModal.open({
                animation: true,
                templateUrl: 'app/pages/outlets/banner.setting.html',
                size: 'lg',
                backdrop: 'static',
                keyboard: false,
                scope: $scope
            });
        };

        var LoadingBox;
        $scope.syncData = function () {
            LoadingBox = $uibModal.open({
                animation: true,
                templateUrl: 'app/pages/Loading.html',
                size: 'sm',
                backdrop: 'static',
                keyboard: false,
                scope: $scope
            });
            OutletsData.sync().then(function (response) {
                toastr.success(response.message, "Success");
                LoadingBox.close();
                OutletsData.load().then(function () {
                    $scope.outletListsMasterData = OutletsData.getList();
                    $scope.outletListsData = [].concat($scope.outletListsMasterData);
                });
            }, function (errorMsg) {
                toastr.error(errorMsg, "Failed");
            })
        };

        var viewDetailsModalBox;
        $scope.viewTemplateInfo = function (item) {
            getTemplateInfo(item.templateId, item.id);
            $scope.outletData = item;
            viewDetailsModalBox = $uibModal.open({
                animation: true,
                templateUrl: 'app/pages/outlets/view.templateInfo.html',
                size: 'md',
                backdrop: 'static',
                keyboard: false,
                scope: $scope
            });
        };
        function getTemplateInfo(templateId, outletId) {
            TemplatesData.templateInfo(templateId, outletId).then(function (response) {
                $scope.templateInfo = response;
            });
        }


        $scope.updateSettings = function (isValid) {
            if (isValid) {
                $scope.newBannerSetting.info.smsGatewayId = $scope.selected.gateway.id;
                if (!$scope.noSmsGateWay.isChecked) {
                    $scope.newBannerSetting.info.smsGatewayId = null;
                }
                OutletsData.updateSettings($scope.outletInfo.id, $scope.newBannerSetting.info).then(function (response) {
                    toastr.success("Banner setting updated successfully", "Success");
                    bannerSettingBox.close();
                    //$scope.newBannerSetting.form.$setPristine();
                    //$scope.newBannerSetting.info=""
                }, function (errorMsg) {
                    toastr.error(errorMsg, "Failed");
                })
            }
        };

        function getOutletInfo(storeId) {
            OutletsData.outletInfo(storeId).then(function (response) {
                $scope.outletInfo = response;
                $scope.newBannerSetting.info.bannerUrl = $scope.outletInfo.bannerUrl;
                $scope.newBannerSetting.info.tableNoRange = $scope.outletInfo.tableNoRange;
                $scope.newBannerSetting.info.mobileNoLength = $scope.outletInfo.mobileNoLength;
                $scope.newBannerSetting.info.pocName = $scope.outletInfo.pocName;
                $scope.newBannerSetting.info.pocEmail = $scope.outletInfo.pocEmail;
                $scope.newBannerSetting.info.pocMobile = $scope.outletInfo.pocMobile;
                $scope.newBannerSetting.info.mgrName = $scope.outletInfo.mgrName;
                $scope.newBannerSetting.info.mgrEmail = $scope.outletInfo.mgrEmail;
                $scope.newBannerSetting.info.mgrMobile = $scope.outletInfo.mgrMobile;
                var selectedGateway = $filter('filter')($scope.smsSettingInfo, {id: $scope.outletInfo.smsGatewayId});
                $scope.selected.gateway = selectedGateway[0];
                if (selectedGateway.length != 0) {
                    $scope.noSmsGateWay.isChecked = true;
                } else {
                    $scope.noSmsGateWay.isChecked = false;
                }

            });
        }

        $scope.noSmsGateWay = {
            isChecked: false
        };


        $scope.selected = {
            outlet: "",
            template: ""
        };

        $scope.newAssignOutlet = {
            form: {},
            info: {
                templateId: "",
                outletId: ""
            }
        };

        $scope.assignOutlet = function (isValid) {
            if (isValid) {
                var outletId = $scope.selected.outlet.id;
                $scope.newAssignOutlet.info.templateId = $scope.selected.template.templateId;
                TemplatesData.assignOutlet(outletId, Template.createAssignQuestionObject($scope.newAssignOutlet.info)).then(function (newdata) {
                    toastr.success("Outlet Assigned successfully", "Success");
                    var outletRow = OutletsData.getRowById($scope.selected.outlet.id);
                    outletRow.templateId = $scope.selected.template.templateId;
                    outletRow.templateName = $scope.selected.template.templateDesc;
                    $scope.newAssignOutlet.form.$setPristine();
                    $scope.newAssignOutlet.info.templateId = "";
                    $scope.newAssignOutlet.info.outletId = "";
                    $scope.selected.outlet = "";
                    $scope.selected.template = ""
                }, function (errorMsg) {
                    toastr.error(errorMsg, "Failed");
                })
            }
        };

        $scope.updateOutlet = function (item) {
            TemplatesData.update(item).then(function (response) {
                toastr.success("Outlet updated successfully", "Success");
            }, function (errorMsg) {
                toastr.error(errorMsg, "Failed");
            });
        };
        $scope.newSelected = {
            gateway: {}
        };
        $scope.newOutlet = {
            form: {},
            info: {
                outletDesc: "",
                shortDesc: "",
                clusterId: "",
                regionId: "",
                companyId: "",
                groupId: "",
                posStoreId: ""
            }
        };

        $scope.newOutletForm = {
            cluster: "",
            region: "",
            company: "",
            group: ""
        };
        $scope.getwayRequired =
            {
                isChecked: false
            };
        $scope.regionDataLoaded = false;
        $scope.clusterDataLoaded = false;
        $scope.companyDataLoaded = false;
        function getGroupList(flag) {
            OutletsData.getGroupList().then(function (response) {
                $scope.GroupsData = response
            });
            if (flag) {
                OutletsData.getCompanyList(0).then(function (response) {
                    $scope.CompanyData = response;
                    $scope.companyDataLoaded = true;
                });
                OutletsData.getClusterList(0).then(function (response) {
                    $scope.ClustersData = response;
                    $scope.clusterDataLoaded = true;
                });
                OutletsData.getRegionList(0).then(function (response) {
                    $scope.RegionsData = response;
                    $scope.regionDataLoaded = true;
                });
            }
        }

        $scope.onRegionChange = function () {
            if ($scope.newOutletForm.region!=null) {
                $scope.clusterDataLoaded = false;
                var regionId = $scope.newOutletForm.region.id;
                OutletsData.getClusterList(regionId).then(function (response) {
                    $scope.ClustersData = response;
                    $scope.clusterDataLoaded = true;
                });
            }
        };
        $scope.onCompanyChange = function () {
            if ($scope.newOutletForm.company!=null) {
                $scope.regionDataLoaded = false;
                var companyId = $scope.newOutletForm.company.id;
                OutletsData.getRegionList(companyId).then(function (response) {
                    $scope.RegionsData = response;
                    $scope.regionDataLoaded = true;
                });
            }
        };
        $scope.onGroupChange = function () {
            if ($scope.newOutletForm.group!=null) {
                $scope.companyDataLoaded = false;
                var groupId = $scope.newOutletForm.group.id;
                OutletsData.getCompanyList(groupId).then(function (response) {
                    $scope.CompanyData = response;
                    $scope.companyDataLoaded = true;
                });
            }
        };

        $scope.createOutlet = function (isValid) {
            if (isValid) {
                $scope.newOutlet.info.clusterId = $scope.newOutletForm.cluster.id;
                $scope.newOutlet.info.regionId = $scope.newOutletForm.region.id;
                $scope.newOutlet.info.groupId = $scope.newOutletForm.group.id;
                $scope.newOutlet.info.companyId = $scope.newOutletForm.company.id;
                $scope.newOutlet.info.posStoreId = ($scope.newOutlet.info.posStoreId).toUpperCase();

                OutletsData.create($scope.newOutlet.info).then(function (response) {
                    toastr.success("Outlet created successfully", "Success");
                    $scope.newOutlet.form.$setPristine();
                    $scope.newOutlet.info = "";
                    $scope.newOutletForm = {};
                    $scope.regionDataLoaded = false;
                    $scope.clusterDataLoaded = false;
                    $scope.companyDataLoaded = false;
                    $location.path("/outlets");
                }, function (errorMsg) {
                    toastr.error(errorMsg, "Failed");
                })
            }
        };
        editableOptions.theme = 'bs3';
        editableThemes['bs3'].submitTpl = '<button type="submit" class="btn btn-primary btn-with-icon"><i class="fa fa-check fa-lg" aria-hidden="true"></i></button>';
        editableThemes['bs3'].cancelTpl = '<button type="button" ng-click="$form.$cancel()" class="btn btn-default btn-with-icon"><i class="fa fa-close fa-lg"></i></button>';

    }


    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('outlets', {
                url: '/outlets',
                controller: outletsCtrl,
                templateUrl: 'app/pages/outlets/outlets.list.html',
                title: 'OUTLETS',
                data: {
                    menuId: 3
                },
                sidebarMeta: {
                    icon: 'fa fa-link',
                    order: 3
                },
                resolve: {
                    "LoadOutlets": function (OutletsData) {
                        return OutletsData.load();
                    },
                    "LoadTemplates": function (TemplatesData) {
                        return TemplatesData.load();
                    }
                }
            })
            .state('createOutlet', {
                url: '/createOutlet',
                templateUrl: 'app/pages/outlets/create.outlet.html',
                controller: outletsCtrl,
                title: 'CREATE OUTLET',
                data: {
                    menuId: 28
                }
            })
    }
})();
