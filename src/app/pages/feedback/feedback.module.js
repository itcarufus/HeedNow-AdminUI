(function () {
    'use strict';

    angular.module('UApps.pages.feedback', [])
        .config(routeConfig).controller('FeedbackCtrl', FeedbackCtrl);

    function FeedbackCtrl($scope, $rootScope, OutletsData, $timeout, FeedbackData, toastr, Feedback, AuthenticationService, Excel, $filter) {

        $scope.outletListsData = $scope.outletListsMasterData = OutletsData.getList();


        /*$scope.generateFeedback = function(){
         var httpService = new HttpService("feedback");
         httpService.post("list").then(function(response){
         $window.open("../feedback/Feedbacks.xls");
         });
         };*/
        $scope.hasFullPermission = AuthenticationService.userHasMenuAccess(23);
        $scope.isLoading = false;
        $scope.selected = {
            outlet: {}
        };
        $scope.excelHeaders = {
            questions: [],
            options: []
        };

        $scope.isValidTable = function isValidTable(val) {
            var regex = /^[-0-9, ]+$/;
            return regex.test(val);

        };

        $scope.generateFeedback = function () {
            $scope.genFeedback.info.outletId = $scope.selected.outlet.id;
            var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
            var firstDate = new Date($scope.genFeedback.info.fromDate);
            var secondDate = new Date($scope.genFeedback.info.toDate);
            var diffDays = Math.round((secondDate.getTime() - firstDate.getTime()) / oneDay);
            var outletIds = [];
            if (diffDays <= 30 && diffDays >= 0) {
                $scope.isLoading = true;
                for (var i = 0; i < $scope.selected.outlet.length; i++) {
                    outletIds.push($scope.selected.outlet[i].id);
                }
                FeedbackData.genFeedback($scope.genFeedback.info, outletIds).then(function (response) {

                    $scope.feedbackListsData = [];
                    $scope.excelHeaders = {
                        questions: [],
                        options: []
                    };

                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {
                            $scope.feedbackListsData.push(response[i]);
                            var oldFeedbackList = response[i].feedbacks;

                            var uniqueQuesid = [];
                            var newFeedbackList = [];
                            for (var j = 0; j < oldFeedbackList.length; j++) {
                                var obj = oldFeedbackList[j];
                                var extraObj = {
                                    "questionId": obj.questionId,
                                    "answerText": obj.answerText,
                                    "answerValue": obj.answerDesc,
                                    "rating": obj.rating,
                                    "questionType": obj.questionType,
                                    "questionDesc": obj.questionDesc,
                                    "answerDesc": obj.answerDesc,
                                    "isNegative": obj.isNegative
                                };

                                if (obj.questionType != "5") {
                                    if (obj.questionType == "2" || obj.questionType == "3") {
                                        extraObj.answerValue = $filter('ratingFilter')(obj.rating);
                                    }
                                    if (obj.questionType == "4") {
                                        extraObj.answerValue = obj.answerText;
                                    }
                                    uniqueQuesid.push(obj.questionId);
                                    newFeedbackList.push(extraObj);
                                } else {
                                    //MultiSelect Ques
                                    if (uniqueQuesid.indexOf(obj.questionId) === -1) {
                                        uniqueQuesid.push(obj.questionId);
                                        newFeedbackList.push(extraObj);
                                    } else {
                                        var existingObj = $filter('filter')(newFeedbackList, {questionId: obj.questionId});
                                        existingObj.answerValue += ", " + obj.answerDesc;
                                    }
                                }
                            }
                            response[i].newFeedbacks = newFeedbackList;
                        }

                        var uniqueNames = [];
                        var uniqueObj = [];
                        var refItem = $scope.feedbackListsData[0].newFeedbacks;
                        for (var j = 0; j < refItem.length; j++) {
                            var obj = refItem[j];
                            if (uniqueNames.indexOf(obj.questionId) === -1) {
                                var quesObj = {
                                    text: obj.questionDesc,
                                    answers: $filter('filter')(refItem, {questionId: obj.questionId})
                                };
                                if (obj.questionType != "3") {
                                    quesObj.answers = []; //Col span for others
                                }
                                uniqueObj.push(quesObj);
                                uniqueNames.push(obj.questionId);
                            }
                            if (obj.questionType == "3") {
                                $scope.excelHeaders.options.push(obj);
                            } else {
                                $scope.excelHeaders.options.push({answerDesc: ""});
                            }
                        }
                        $scope.excelHeaders.questions = uniqueObj;

                        $timeout(function () {
                            $scope.isLoading = false;
                            Excel.tableToExcel("#tableToExport", 'FeedbackReport', 'FeedbackReport.xls');
                        }, 1000); // trigger download
                    } else {
                        $scope.isLoading = false;
                    }
                    $scope.selected.outlet = {};
                    $scope.genFeedback.form.$setPristine();
                    $scope.genFeedback.info = "";
                }, function (errorMsg) {
                    $scope.isLoading = false;
                    toastr.error(errorMsg, "No Data");
                });
            }
            else {
                if (diffDays < 0) {
                    toastr.info("FROM date cannot be greater than TO date!");
                } else {
                    toastr.info("Feedback report cannot be pulled for more than one month!");
                }

            }
        };


        $scope.ifItsNegative = function (isNegative) {
            if (isNegative == 1) {
                return {
                    'background-color': '#D8000C',
                    'color': '#FFFFFF'
                }
            }

        };


        var userId = $rootScope.globals.currentUser.userId;
        $scope.genFeedback = {
            form: {},
            info: {
                userId: userId,
                fromDate: "",
                toDate: "",
                tableNo: "",
            }
        };


        $scope.open1 = function () {
            if ($scope.genFeedback.info.fromDate.getMonth) {
                $scope.genFeedback.info.toDate = $scope.genFeedback.info.fromDate;
                setFirstAndLastDate($scope.genFeedback.info.fromDate);
            }
            $scope.popup1.opened = true;
        };
        $scope.popup1 = {
            opened: false
        };
        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };
        $scope.popup2 = {
            opened: false
        };
        $scope.enableDisablePopUp = function () {
            if ($scope.genFeedback.info.fromDate != "" && angular.isDefined($scope.genFeedback.info.fromDate)) {
                $scope.genFeedback.info.toDate = "";
                $scope.ifFormDateNotSelected = false;
            } else {
                $scope.genFeedback.info.toDate = "";
                $scope.ifFormDateNotSelected = true;
            }
        };
        $scope.format = 'dd-MMMM-yyyy';


        setFirstAndLastDate(new Date());

        function setFirstAndLastDate(dt) {
            var date = dt, y = date.getFullYear(), m = date.getMonth();
            var firstDay = new Date(y, m, 1);
            var lastDay = new Date(y, m + 1, 0);
            $scope.minDate = firstDay;
            $scope.maxDate = lastDay;
        }
    }


    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('feedback', {
                url: '/feedback',
                controller: FeedbackCtrl,
                templateUrl: 'app/pages/feedback/feedback.html',
                title: 'FEEDBACK',
                data: {
                    menuId: 4
                },
                sidebarMeta: {
                    icon: 'fa fa-list-ul',
                    order: 4
                },
                resolve: {
                    "LoadOutlets": function (OutletsData) {
                        return OutletsData.load();
                    }
                }
            })
    }
})();
