
(function () {
    'use strict';

    angular.module('UApps.auth').controller('RegisterController',
        ['$scope', '$rootScope', '$location', 'toastr', 'ClientsData',
            function ($scope, $rootScope, $location, toastr, ClientsData) {



                $scope.newClient = {
                    form: {},
                    info: {
                        name: "",
                        mobile: "",
                        location: "",
                        email: "",
                        userName: "",
                        password: ""
                    }
                };

                $scope.createClient = function (isValid) {
                    if (isValid) {
                        ClientsData.create($scope.newClient.info).then(function (response) {
                            toastr.success("User registration is successful", "Success");
                            $scope.newClient.form.$setPristine();
                            $scope.newClient.info = "";
                            $location.path('/login');
                        }, function (errorMsg) {
                            toastr.error("Failed to register.Try Again!", "Failed");
                        });
                    }
                };


            }]).config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('register', {
                url: '/register',
                views: {
                    'auth': {
                        controller: 'RegisterController',
                        templateUrl: 'app/client/register.html'
                    }
                }
            });
    }
})();






