/**
 * Created by Sandeep on 9/9/2016.
 */
(function () {
    'use strict';

    angular.module('UApps.services')
        .factory("OutletServices", OutletServices)
        .service("OutletsData", OutletsData)
        .service("Outlet", Outlet);

    /** @ngInject */
    function OutletServices(HttpService) {

        var httpService = new HttpService("outlet");
        var httpSettingService = new HttpService("settings");
        var httpClusterService = new HttpService("cluster");
        var httpGroupService = new HttpService("group");
        var httpRegionService = new HttpService("region");
        var httpCompanyService = new HttpService("company");
        var OutletServices = {
            create: function (obj) {
                return httpService.post("create", obj);
            }, updateSettings: function (outletId, obj) {
                return httpService.post("updateSettings/" + outletId, obj);
            }, getList: function (obj) {
                return httpService.post("list/", obj);
            }, outletInfo: function (storeId) {
                return httpService.get("outletInfoById/" + storeId);
            }, sync: function () {
                return httpSettingService.get("sync");
            }, getAllOutlet: function (obj) {
                return httpService.post("list");
            }, clusterList: function (regionId) {
                return httpClusterService.get("list/"+regionId);
            }, groupList: function () {
                return httpGroupService.get("list");
            }, regionList: function (companyId) {
                return httpRegionService.get("list/"+companyId);
            }, companyList: function (groupId) {
                return httpCompanyService.get("list/"+groupId);
            }
        };
        return OutletServices;
    }

    /** @ngInject */
    function OutletsData(OutletServices, $filter, $rootScope, DataObject) {

        var OutletsData = new DataObject();
        OutletsData.load = function () {
            var self = this;

            var postData = {
                userId: parseInt($rootScope.globals.currentUser.userId),
                outletId: $rootScope.globals.currentUser.outletAccess
            };
            return OutletServices.getList(postData).then(function (data) {
                var sortedList = $filter('orderBy')(data.outletResponseList, 'outletDesc', false);
                return self.rows = sortedList;

            });
        };

        OutletsData.create = function (obj) {
            var self = this;
            if (obj.hasOwnProperty("id"))
                delete obj.id;
            return OutletServices.create(obj).then(function (data) {
                obj.id = data.message;
                self.rows.push(obj);
                return self.rows;
            });
        };
        OutletsData.outletInfo = function (storeId) {
            var self = this;
            return OutletServices.outletInfo(storeId).then(function (data) {
                return data;
            });
        };
        OutletsData.sync = function () {
            var self = this;
            return OutletServices.sync().then(function (data) {
                return data;
            });
        };
        OutletsData.updateSettings = function (outletId, obj) {
            return OutletServices.updateSettings(outletId, obj).then(function (data) {
                return data;
            });
        };

        OutletsData.getClusterList = function (regionId) {
            return OutletServices.clusterList(regionId).then(function (data) {
                return data.clusters;
            });
        };
        OutletsData.getGroupList = function () {
            return OutletServices.groupList().then(function (data) {
                return data.groups;
            });
        };
        OutletsData.getRegionList = function (companyId) {
            return OutletServices.regionList(companyId).then(function (data) {
                return data.regions;
            });
        };
        OutletsData.getCompanyList = function (groupId) {
            return OutletServices.companyList(groupId).then(function (data) {
                return data.clusters;
            });
        };
        return OutletsData;
    }

    /** @ngInject */
    function Outlet() {
        var Outlet = {
            newObject: function () {
                return {
                    id: undefined,
                    outletDesc: undefined,
                    outletType: undefined,
                    parentAnswerId: "",
                    parentOutletId: "",
                    answerOption: undefined,
                    answerSymbol: undefined
                }
            },
            updateObject: function (obj) {
                return {
                    id: obj.id,
                    bannerLink: obj.bannerLink,
                    tableNo: obj.tableNo
                }
            }

        };
        return Outlet;

    }


})();
