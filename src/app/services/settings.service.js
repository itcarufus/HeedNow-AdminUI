/**
 * Created by Sandeep on 9/9/2016.
 */
(function () {
    'use strict';

    angular.module('UApps.services')
        .factory("SettingsServices", SettingsServices)
        .service("SettingsData", SettingsData)
        .service("RolesData", RolesData)
        .service("Role", Role)
        .service("_Setting", _Setting);


    /** @ngInject */
    function SettingsServices(HttpService) {

        var httpService = new HttpService("settings");

        var httpUserService = new HttpService("user");

        var _SettingsServices = {
            create: function (obj) {
                return httpUserService.post("createRole", obj);
            }, updateRole: function (obj) {
                return httpUserService.post("updateRole", obj);
            }, fetch: function () {
                return httpService.get("fetch");
            }, saveSettings: function (obj) {
                return httpService.post("save", obj);
            }, fetchSmsSetting: function () {
                return httpService.get("fetchSmsSettings");
            }, saveSmsSettings: function (obj) {
                return httpService.post("saveSmsSettings", obj);
            }, updateSmsSettings: function (obj) {
                return httpService.post("updateSmsSettings", obj);
            }, menuList: function () {
                return httpUserService.get("menuList");
            }, getList: function () {
                return httpUserService.get("roleList");
            }
        };
        return _SettingsServices;
    }

    /** @ngInject */
    function SettingsData(SettingsServices,DataObject, $filter) {

        //var _SettingsData = {data: {}};

        var _SettingsData = new DataObject();
        _SettingsData.fetch = function () {
            var self = this;
            return SettingsServices.fetch().then(function (data) {
                return data;
            });
        };

        _SettingsData.saveSettings = function (obj) {
            return SettingsServices.saveSettings(obj).then(function (data) {
                return data;
            });
        };
        _SettingsData.saveSmsSettings = function (obj) {
            return SettingsServices.saveSmsSettings(obj).then(function (data) {
                return data;
            });
        };
        _SettingsData.fetchSmsSetting = function () {
            return SettingsServices.fetchSmsSetting().then(function (data) {
                return data.smsSettings;
            });
        };
        _SettingsData.updateSmsSettings = function (obj) {
            return SettingsServices.updateSmsSettings(obj).then(function (data) {
                return data;
            });
        };
     /*   _SettingsData.getData = function () {
            return this.data;
        };*/

        _SettingsData.menuList = function () {
            return SettingsServices.menuList().then(function (data) {
                return data.menus
            });
        };


        return _SettingsData;
    }

    function RolesData(SettingsServices, DataObject) {

        var RolesData = new DataObject();

        RolesData.load = function () {
            var self = this;
            return SettingsServices.getList().then(function (data) {
                return self.rows = data.roles;
            });
        };
        RolesData.create = function (obj) {
            var self = this;
            if (obj.hasOwnProperty("roleId"))
                delete obj.roleId;
            return SettingsServices.create(obj).then(function (data) {
                obj.roleId = data.message;
                self.rows.push(obj);
                return self.rows;
            });
        };
        RolesData.update = function (obj) {
            var self = this;
            return SettingsServices.updateRole(obj).then(function (data) {
                var index = self.getRowIndexById(obj.roleId);
                self.rows[index] = obj;
                return data;
            });
        };

        return RolesData;
    }

    function Role() {

        var Role = {
            updateObject: function (obj, menuStr, outletStr, isAll) {
                return {
                    roleId: obj.roleId,
                    name: obj.name,
                    menuAccess: menuStr,
                    outletAccess: outletStr,
                    isAll: isAll,
                }
            },
            createObject: function (name, menuStr, outletStr, isAll) {
                return {
                    name: name,
                    menuAccess: menuStr,
                    outletAccess: outletStr,
                    isAll: isAll,
                }
            }
        };
        return Role;
    }

    function _Setting() {
        var _Setting = {
            saveObject: function (obj) {
                var d1 = new Date(obj.archiveTime);
                var hh = "0" + d1.getHours();
                var mm = "0" + d1.getMinutes();
                var ss = "0" + d1.getSeconds();
                var at = hh.substr(-2) + ":" + mm.substr(-2) + ":" + ss.substr(-2);
                var d2 = new Date(obj.reportTime);
                var hh1 = "0" + d2.getHours();
                var mm1 = "0" + d2.getMinutes();
                var ss1 = "0" + d2.getSeconds();
                var rt = hh1.substr(-2) + ":" + mm1.substr(-2) + ":" + ss1.substr(-2);
                return {
                    smsTemplate: obj.smsTemplate,
                    archiveTime: at,
                    reportTime: rt,
                    emailTemplate: obj.emailTemplate,
                    versionCode: obj.versionCode,
                    versionName: obj.versionName
                }
            }
        };
        return _Setting;
    }

})();
