/**
 * Created by Sandeep on 9/9/2016.
 */
(function () {
    'use strict';

    angular.module('UApps.services')
        .factory("UserServices", UserServices)
        .factory("ClientServices", ClientServices)
        .service("UsersData", UsersData)
        .service("ClientsData", ClientsData)
        .service("User", User)
        .service("Client", Client);


    /** @ngInject */
    function UserServices(HttpService) {

        var httpService = new HttpService("user");

        var UserServices = {
            create: function (obj) {
                return httpService.post("create", obj);
            }, getList: function () {
                return httpService.get("list");
            }, update: function (obj) {
                return httpService.post("update", obj);
            }, userInfo: function (userId) {
                return httpService.get("userInfo/" + userId);
            }, resetPassword: function (obj) {
                return httpService.post("resetPassword", obj);
            }
        };
        return UserServices;
    }

    function ClientServices(HttpService) {
        var httpClientService = new HttpService("client");
        var ClientServices = {
            create: function (obj) {
                return httpClientService.post("create", obj);
            }, getClientsList: function () {
                return httpClientService.get("list");
            }, clientInfo: function (clientId) {
                return httpClientService.get("clientInfo/" + clientId);
            }
        };
        return ClientServices;

    }

    /** @ngInject */
    function UsersData(UserServices, User, DataObject) {

        var UsersData = new DataObject();
        UsersData.load = function () {
            var self = this;
            return UserServices.getList().then(function (data) {
                return self.rows = data.users;
            });
        };

        UsersData.create = function (obj) {
            var self = this;
            if (obj.hasOwnProperty("id"))
                delete obj.id;
            return UserServices.create(obj).then(function (data) {
                obj.id = data.message;
                self.rows.push(obj);
                return self.rows;
            });
        };
        UsersData.update = function (obj) {
            var self = this;
            return UserServices.update(User.updateObject(obj)).then(function (data) {
                var index = self.getRowIndexById(obj.id);
                self.rows[index] = obj;
                return data;
            });
        };

        UsersData.userInfo = function (userId) {
            return UserServices.userInfo(userId).then(function (data) {
                return data;
            });
        };
        UsersData.resetPassword = function (obj) {
            return UserServices.resetPassword(obj).then(function (data) {
                return data
            });
        };

        return UsersData;
    }

    function ClientsData(ClientServices, DataObject) {
        var ClientsData = new DataObject();

        ClientsData.create = function (obj) {
            var self = this;
            if (obj.hasOwnProperty("id"))
                delete obj.id;
            return ClientServices.create(obj).then(function (data) {
                obj.id = data.message;
                self.rows.push(obj);
                return self.rows;
            });
        };
        ClientsData.clientList = function () {
            return ClientServices.getClientsList().then(function (data) {
                return data.clients;
            });
        };

        ClientsData.clientInfo = function (clientId) {
            return ClientServices.clientInfo(clientId).then(function (data) {
                return data;
            });
        };

        return ClientsData;
    }

    function User() {

        var User = {
            updateObject: function (obj) {
                return {
                    id: obj.id,
                    name: obj.name,
                    email: obj.email,
                    status: obj.status,
                    role: obj.role.toString(),
                    notifyEmail:obj.notifyEmail
                }
            }
        };
        return User;
    }
    function Client() {

        var Client = {
            newObject: function () {
                return {
                    clientId:undefined,
                    name: undefined,
                    mobile: undefined,
                    location: undefined,
                    email: undefined,
                    noOfDevices: undefined,
                    noOfOutlets: undefined,
                    userName: undefined,
                    userEmail: undefined,
                    password: undefined
                }
            }
        };
        return Client;
    }

})();
