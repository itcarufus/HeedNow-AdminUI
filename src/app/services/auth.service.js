/**
 * Created by Sandeep on 9/9/2016.
 */
(function () {
    'use strict';


    angular.module('UApps.services')
        .factory("AuthenticationService", AuthenticationService);


    /** @ngInject */
    function AuthenticationService(Base64, $http, $q, HttpService, $cookieStore, $rootScope) {
        var httpService = new HttpService("user");

        var AuthenticationService = {};

        AuthenticationService.authenticate = function () {
            return angular.isDefined($rootScope.globals.currentUser)
                && angular.isDefined($rootScope.globals.currentUser.userId)
                && angular.isDefined($rootScope.globals.currentUser.authData)
                && angular.isDefined($rootScope.globals.currentUser.sessionId)
                && angular.isDefined($rootScope.globals.currentUser.clientId)
                && angular.isDefined($rootScope.globals.currentUser.roles);
        };

        AuthenticationService.getUserId = function () {
            return angular.isDefined($rootScope.globals.currentUser.userId);
        };
        AuthenticationService.getClientId = function () {
            return angular.isDefined($rootScope.globals.currentUser.clientId);
        };

        AuthenticationService.getUserRole = function () {
            return $rootScope.globals.currentUser.roles;
        };

        AuthenticationService.userHasMenuAccess = function (menuId) {
            var self = this;
            if (!AuthenticationService.authenticate())
                return false;
            var menuAccessArr = self.getUserRole().menu.split(",");
            if (menuAccessArr.indexOf(menuId.toString()) >= 0) {
                return true;
            }
            else if (menuId == "D") {
                return true
            }
        };

        AuthenticationService.init = function () {
            $rootScope.globals = $cookieStore.get('globals') || {};
            var cookieUser = $rootScope.globals.currentUser;
            if ($rootScope.globals.currentUser) {
                if (angular.isUndefined(cookieUser.userId)
                    || angular.isUndefined(cookieUser.authData)
                    || angular.isUndefined(cookieUser.sessionId)
                    || angular.isUndefined(cookieUser.roles)
                    || angular.isUndefined(cookieUser.clientId)
                    || angular.isUndefined(cookieUser.outletAccess)) {
                    AuthenticationService.clearCredentials();
                }
            }
        };

        AuthenticationService.getUserInfo = function () {
            var cookieUser = $rootScope.globals.currentUser;
            // $rootScope.globals = {};
            $http.defaults.headers.common['Auth'] = cookieUser.authData; // jshint ignore:line
            $http.defaults.headers.common['sessionId'] = cookieUser.sessionId; // jshint ignore:line
            return httpService.get('userInfo/' + cookieUser.userId).then(function (response) {
                return response;
            });
        };

        AuthenticationService.login = function (username, password,clientId) {
            var self = this;
            return httpService.post('login', {
                "userName": username,
                "password": password,
                "clientId": clientId
            });

            /*  //OFFLINE LOGIN
             var loginResponse = {
             message : "1280918200@1"
             }
             return $q.resolve(loginResponse);*/
        };

        AuthenticationService.logout = function () {
            return httpService.post('logout', {
                "userId": AuthenticationService.getUserId()
            });
        };

        AuthenticationService.setCredentials = function (username, password, sessionId, response) {
            var sessionTimeStamp = sessionId.split("@")[0];
            var clientId = response.clientId;
            var authData = Base64.encode(username + ':' + password + ":" + sessionTimeStamp + ":" + clientId);

                $rootScope.globals = {
                    currentUser: {
                        userId: sessionId.split("@")[1],
                        authData: authData,
                        sessionId: sessionId,
                        isFeedbackView: false,
                        roles: {
                            menu: response.menuAccess
                        },
                        outletAccess: response.outletAccess,
                        clientId: clientId
                    }
                };

            $http.defaults.headers.common['Auth'] = authData; // jshint ignore:line
            $http.defaults.headers.common['sessionId'] = sessionId; // jshint ignore:line
            $cookieStore.put('globals', $rootScope.globals);
        };

        AuthenticationService.clearCredentials = function () {
            $rootScope.globals = {};
            $cookieStore.remove('globals');
            $http.defaults.headers.common.Authorization = '';
        };

        return AuthenticationService;

    }


})();