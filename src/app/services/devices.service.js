/**
 * Created by Sandeep on 9/9/2016.
 */
(function () {
    'use strict';

    angular.module('UApps.services')
        .factory("DeviceServices", DeviceServices)
        .service("DevicesData", DevicesData)
        .service("Device", Device);

    /** @ngInject */
    function DeviceServices(HttpService) {

        var httpService = new HttpService("device");
        var DeviceServices = {
            create: function (obj) {
                return httpService.post("create", obj);
            }, update: function (obj) {
                return httpService.post("update", obj);
            }, getList: function () {
                return httpService.get("list");
            }

        };
        return DeviceServices;
    }

    /** @ngInject */
    function DevicesData(DeviceServices, Device, DataObject) {

        var DevicesData = new DataObject();
        DevicesData.load = function () {
            var self = this;
            return DeviceServices.getList().then(function (data) {
                return self.rows = data.devices;

            });
        };
        DevicesData.create = function (obj) {
            var self = this;
            if (obj.hasOwnProperty("id"))
                delete obj.id;
            return DeviceServices.create(obj).then(function (data) {
                obj.id = data.message;
                self.rows.push(obj);
                return self.rows;
            });
        };
        DevicesData.update = function (obj) {
            var self = this;
            return DeviceServices.update(Device.updateObject(obj)).then(function (data) {
                var index = self.getRowIndexById(obj.id);
                self.rows[index] = obj;
                return data;
            });
        };
        return DevicesData;
    }

    /** @ngInject */
    function Device() {
        var Device = {
            newObject: function () {
                return {
                    id: undefined,
                    macAdd: undefined,
                    serialNo: "",
                    model: "",
                    androidVersion: "",
                    installationDate: ""
                }
            },
            updateObject: function (obj,clientId) {
                return {
                    androidDeviceId: obj.androidDeviceId,
                    status: obj.status,
                    clientId: clientId
                }
            }
        };
        return Device;

    }


})();
