/**
 * Created by Sandeep on 9/9/2016.
 */
(function () {
    'use strict';

    angular.module('UApps.services', [])
        .factory('HttpService', function ($http, $q,$rootScope, $cookieStore,$location, toastr) {
            var apiRoot = "/heednow-1.0/";

            var HttpService = function (apiModule) {
                this.apiModule = apiModule;
            };

            function makeRequestSuccess(response) {
                if (response.data.messageType == "SUCCESS") {
                    return response.data;
                } else if (response.data.messageType == "UNAUTHORIZED") {
                    if ($rootScope.globals.currentUser) {
                        $location.path("/login");
                        $rootScope.globals = {};
                        $cookieStore.remove('globals');
                        $http.defaults.headers.common['Auth'] = ""; // jshint ignore:line
                        $http.defaults.headers.common['sessionId'] = ""; // jshint ignore:line
                    }
                    return $q.reject("Unauthorized access!");
                } else {
                    return $q.reject(response.data.message);
                }
            }

            function makeRequestFailed(response) {
                var errMsg = "Some problem in server, try reloading the page. If the issue still persist contact admin.";
                return $q.reject("Error#" + response.status + ": " + errMsg);
            }

            HttpService.prototype.get = function (url) {
                var self = this;
                return $http.get(apiRoot + self.apiModule + "/" + url).then(makeRequestSuccess, makeRequestFailed);
            };
            HttpService.prototype.post = function (url, params) {
                var self = this;
                return $http.post(apiRoot + self.apiModule + "/" + url, params).then(makeRequestSuccess, makeRequestFailed);
            };
            HttpService.prototype.delete = function (url) {
                var self = this;
                return $http.delete(apiRoot + self.apiModule + "/" + url).then(makeRequestSuccess, makeRequestFailed);
            };
            return HttpService;
        })
        .factory('DataObject', function () {
            var DataObject = function () {
                this.rows = [];
            };
            DataObject.prototype = {
                getList: function () {
                    return this.rows;
                },
                getRowById: function (id, safedata) {
                    var self = this;
                    for (var i in self.rows) {
                        if (self.rows[i].id == id) {
                            return self.rows[i];
                        }
                    }
                    return safedata;
                },
                getRowIndexById: function (id) {
                    var self = this;
                    for (var i in self.rows) {
                        if (self.rows[i].id == id) {
                            return i;
                        }
                    }
                    return -1;
                }

            };

            return DataObject;
        })
        .factory('Base64', function () {
            /* jshint ignore:start */

            var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

            return {
                encode: function (input) {
                    var output = "";
                    var chr1, chr2, chr3 = "";
                    var enc1, enc2, enc3, enc4 = "";
                    var i = 0;

                    do {
                        chr1 = input.charCodeAt(i++);
                        chr2 = input.charCodeAt(i++);
                        chr3 = input.charCodeAt(i++);

                        enc1 = chr1 >> 2;
                        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                        enc4 = chr3 & 63;

                        if (isNaN(chr2)) {
                            enc3 = enc4 = 64;
                        } else if (isNaN(chr3)) {
                            enc4 = 64;
                        }

                        output = output +
                            keyStr.charAt(enc1) +
                            keyStr.charAt(enc2) +
                            keyStr.charAt(enc3) +
                            keyStr.charAt(enc4);
                        chr1 = chr2 = chr3 = "";
                        enc1 = enc2 = enc3 = enc4 = "";
                    } while (i < input.length);

                    return output;
                }
            };

            /* jshint ignore:end */
        })
    .factory('Excel', function ($window, Base64) {
        var uri = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8;base64,',
            template = '<html xmlns:v="urn:schemas-microsoft-com:vml"  xmlns:o="urn:schemas-microsoft-com:office:office"  xmlns:x="urn:schemas-microsoft-com:office:excel"  xmlns="http://www.w3.org/TR/REC-html40">' +
                '<head><style>font-family{font-size:10.0pt; font-family:Verdana, sans-serif;  mso-font-charset:0;} th, td {border:.1pt solid black;}</style>' +
                '<!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
            base64 = function (s) {
                return $window.btoa(unescape(encodeURIComponent(s)));
            },
            format = function (s, c) {
                return s.replace(/{(\w+)}/g, function (m, p) {
                    return c[p];
                })
            };
        return {
            tableToExcel: function (tableId, worksheetName, fileName) {
                /*var table = $(tableId),
                 ctx = {worksheet: worksheetName, table: table.html()},
                 href = uri + base64(format(template, ctx));
                 return href;*/
                var table = $(tableId),
                    template = '<html xmlns:v="urn:schemas-microsoft-com:vml"  xmlns:o="urn:schemas-microsoft-com:office:office"  xmlns:x="urn:schemas-microsoft-com:office:excel"  xmlns="http://www.w3.org/TR/REC-html40">' +
                        '<head><style>font-family{font-size:10.0pt; font-family:Verdana, sans-serif;  mso-font-charset:0;} th, td {border:.1pt solid black;}</style>' +
                        '<!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>'+worksheetName+'</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>' + table.html() + '</table></body></html>';
                var blob = new Blob([template], {
                    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                });
                saveAs(blob, fileName);

            }
        };
    });

})();
