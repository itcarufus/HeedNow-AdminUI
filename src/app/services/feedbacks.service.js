/**
 * Created by Sandeep on 9/9/2016.
 */
(function () {
    'use strict';

    angular.module('UApps.services')
        .factory("FeedbackServices", FeedbackServices)
        .service("FeedbackData", FeedbackData)
        .service("ReportData", ReportData)
        .service("Feedback", Feedback);


    /** @ngInject */
    function FeedbackServices(HttpService) {

        var httpService = new HttpService("feedback");
        var httpReportService = new HttpService("report");
        var FeedbackServices = {
            getList: function () {
                return httpService.get("list");
            },
            genFeedback: function (obj) {
                return httpService.post("list", obj);
            },
            genNegativeFeedbackReport: function (obj) {
                return httpService.post("negativeReport?isNegative=1", obj);
            },
            genDashBoardFeedbackReport: function (obj) {
                return httpService.post("negativeReport", obj);
            },
            feedbackDetails: function (feedbackId) {
                return httpService.get("feedbackDetail/" + feedbackId);
            },
            createFeedbackTracking: function (obj) {
                return httpService.post("createfeedbackTracking", obj);
            },
            questionAvgRatingById: function (questionId) {
                return httpReportService.get("questionRating/average/" + questionId);
            },
            questionRatingCountById: function (questionId) {
                return httpReportService.get("questionRating/count/" + questionId);
            },
            dailyReportByUserId: function (userId) {
                return httpReportService.get("dailyReport/" + userId);
            }
        };
        return FeedbackServices;
    }

    /** @ngInject */
    function FeedbackData(FeedbackServices, Feedback, DataObject) {

        var FeedbackData = new DataObject();
        FeedbackData.load = function () {
            var self = this;
            return FeedbackServices.getList().then(function (data) {
                return self.rows = data;

            });
        };

        FeedbackData.genFeedback = function (obj, outletIds) {
            return FeedbackServices.genFeedback(Feedback.genFeedbackObj(obj, outletIds)).then(function (data) {
                return data.feedbacks;
            });
        };
        FeedbackData.feedbackDetails = function (feedbackId) {
            return FeedbackServices.feedbackDetails(feedbackId).then(function (data) {
                return data;
            });
        };

        return FeedbackData;
    }

    /** @ngInject */
    function ReportData(FeedbackServices, Feedback, $rootScope,DataObject) {

        var ReportData = new DataObject();
        ReportData.load = function () {
            var self = this;
            return FeedbackServices.getList().then(function (data) {
                return self.rows = data;

            });
        };
        ReportData.genDashBoardFeedbackReport = function (obj, outletIds, overrideDate) {
            var postData = Feedback.genFeedbackObj(obj, outletIds);
            if (angular.isDefined(overrideDate) && overrideDate) {
                postData = Feedback.genFeedbackObjAsIs(obj, outletIds);
            }
            return FeedbackServices.genDashBoardFeedbackReport(postData).then(function (data) {
                return data.feedbackTrackingDetails;
            });
        };
        ReportData.genNegativeFeedbackReport = function (obj, outletIds) {
            var postData = Feedback.genFeedbackObj(obj, outletIds);
            return FeedbackServices.genNegativeFeedbackReport(postData).then(function (data) {
                return data.feedbackTrackingDetails;
            });
        };

        ReportData.createFeedbackTracking = function (feedbackId) {
            var obj = {
                feedbackId: feedbackId
            };
            return FeedbackServices.createFeedbackTracking(obj).then(function (data) {
                return data
            })
        };
        ReportData.questionAvgRatingById = function (questionId) {
            return FeedbackServices.questionAvgRatingById(questionId).then(function (data) {
                return data.average;
            });
        };
        ReportData.questionRatingCountById = function (questionId) {
            return FeedbackServices.questionRatingCountById(questionId).then(function (data) {
                return data.count;
            });
        };
        ReportData.dailyReportByUserId = function () {
            var userId=$rootScope.globals.currentUser.userId;
            return FeedbackServices.dailyReportByUserId(userId).then(function (data) {
                return data;
            });
        };
        return ReportData;

    }

    function Feedback($filter) {
        var Feedback = {
            genFeedbackObj: function (obj, outletIds) {
                return {
                    userId: obj.userId,
                    fromDate: $filter('date')(obj.fromDate, 'yyyy-MM-dd 00:00:00'),
                    toDate: $filter('date')(obj.toDate, 'yyyy-MM-dd 23:59:59'),
                    outletId: outletIds,
                    tableNo: obj.tableNo
                }
            },
            genFeedbackObjAsIs: function (obj, outletIds) {
                var myDate = new Date();
                var previousDay = new Date(myDate);
                previousDay.setDate(myDate.getDate()-1);
                var fromDate=$filter('date')(previousDay, 'yyyy-MM-dd 00:00:00');
                return {
                    userId: obj.userId,
                    fromDate: fromDate,
                    toDate: $filter('date')(obj.toDate, 'yyyy-MM-dd HH:mm:ss'),
                    outletId: outletIds,
                    tableNo: obj.tableNo
                }
            }
        };
        return Feedback;
    }

})();
