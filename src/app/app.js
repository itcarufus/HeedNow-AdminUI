'use strict';

angular.module('UApps', [
    'ngAnimate',
    'ui.bootstrap',
    'ui.sortable',
    'ui.router',
    'ngTouch',
    'ngCookies',
    'toastr',
    'smart-table',
    "xeditable",
    'ui.slimscroll',
    'angular-progress-button-styles',
    'ui.select',
    'ngSanitize',
    'chart.js',
    'ui.validate',
    'ngJsonExportExcel',
    'angular-confirm',
    'checklist-model',
    'reCAPTCHA',

    'UApps.services',
    'UApps.constants',
    'UApps.theme',
    'UApps.auth',
    'UApps.filters',
    'UApps.pages'

]);