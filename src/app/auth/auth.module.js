/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('UApps.auth', ['UApps.services']).controller('LoginController',
        ['$scope', '$rootScope', '$location', 'AuthenticationService', 'toastr', 'ClientsData',
            function ($scope, $rootScope, $location, AuthenticationService, toastr, ClientsData) {
                $rootScope.registerPage = false;
                $scope.beforeAccountId = true;
                $scope.afterAccountId = false;

                $scope.userLoginInfo = {
                    userName: "",
                    userPassword: "",
                    clientId: ""
                };
                $scope.clientInfo = {
                    clientId: ""
                };
                $scope.loginForm = {
                    userName: "",
                    userPassword: ""
                };
                $scope.onNextButton = function (isValid) {
                    if (isValid) {
                        $scope.beforeAccountId = false;
                        $scope.afterAccountId = true;
                    }
                };
                $scope.onBackNextButton = function () {
                    $scope.userLoginInfo.userName = "";
                    $scope.userLoginInfo.userPassword = "";
                    $scope.beforeAccountId = true;
                    $scope.afterAccountId = false;
                };

                /*  getClientsList();
                 function getClientsList(){
                 ClientsData.clientList().then(function (response) {
                 $scope.ClientsData=response;
                 });
                 }*/
                // reset login status
                AuthenticationService.clearCredentials();

                $scope.login = function (isValid) {
                    if (isValid) {
                        $scope.dataLoading = true;
                        AuthenticationService.login($scope.userLoginInfo.userName, $scope.userLoginInfo.userPassword, $scope.userLoginInfo.clientId).then(function (response) {
                            $scope.dataLoading = false;
                            AuthenticationService.setCredentials($scope.userLoginInfo.userName, $scope.userLoginInfo.userPassword, response.message, response);
                            $rootScope.afterLogin = true;
                            $location.path('/dashboard');
                            $scope.beforeAccountId = true;
                            $scope.afterAccountId = false;
                        }, function (error) {
                            $scope.dataLoading = false;
                            toastr.error(error, "Error");
                        });
                    }
                };

            }]).config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('auth', {
                url: '/login',
                views: {
                    'auth': {
                        controller: 'LoginController',
                        templateUrl: 'app/auth/auth.html'
                    }
                },
                title: ''
            });
    }
})();
